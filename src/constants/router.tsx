import APIRolesPage from 'pages/APIRolesPage';
import CreateAPIRolePage from 'pages/APIRolesPage/components/CreateAPIRolePage';
import UpdateAPIRolePage from 'pages/APIRolesPage/components/UpdateAPIRolePage';
import APIsPage from 'pages/APIsPage';
import CreateAPIsPage from 'pages/APIsPage/components/CreateAPIsPage';
import UpdateAPIsPage from 'pages/APIsPage/components/UpdateAPIsPage';
import DashboardAdminPage from 'pages/DashboardAdminPage';
import DashboardPage from 'pages/DashboardPage';
import EmptyPage from 'pages/EmptyPage';

import KeyManagementPage from 'pages/KeyManagementPage';
import NodeKeyPage from 'pages/NodeKeyPage';
import OrganizationsPage from 'pages/OrganizationsPage';
import CreateOrganizationPage from 'pages/OrganizationsPage/components/CreateOrganizationPage';
import UpdateOrganizationPage from 'pages/OrganizationsPage/components/UpdateOrganizationPage';
import RolesPage from 'pages/RolesPage';
import CreateRolesPage from 'pages/RolesPage/components/CreateRolesPage';
import UpdateRolesPage from 'pages/RolesPage/components/UpdateRolesPage';
import UserPage from 'pages/UsersPage';
import CreateUserPage from 'pages/UsersPage/components/CreateUserPage';
import UpdateUserPage from 'pages/UsersPage/components/UpdateUserPage';
import ViewRolesPage from 'pages/ViewRolesPage';
import CreateViewRolesPage from 'pages/ViewRolesPage/components/CreateViewRolesPage';
import UpdateViewRolesPage from 'pages/ViewRolesPage/components/UpdateViewRolesPage';
import ViewPage from 'pages/ViewsPage';
import CreateViewPage from 'pages/ViewsPage/components/CreateViewPage';
import UpdateViewPage from 'pages/ViewsPage/components/UpdateViewPage';

export const ROUTES = [
  {
    path: '/',
    component: <EmptyPage />,
  },

  // ORGANIZATIONS

  {
    path: '/organizations/update/:id',
    component: <UpdateOrganizationPage />,
  },

  {
    path: '/organizations/create',
    component: <CreateOrganizationPage />,
  },
  {
    path: '/organizations',
    component: <OrganizationsPage />,
  },
  // USERS
  {
    path: '/users/update-user/:id',
    component: <UpdateUserPage />,
  },
  {
    path: '/users/create-user',
    component: <CreateUserPage />,
  },
  {
    path: '/users',
    component: <UserPage />,
  },
  // ROLES
  {
    path: '/roles/update/:id',
    component: <UpdateRolesPage />,
  },
  {
    path: '/roles/create',
    component: <CreateRolesPage />,
  },
  {
    path: '/roles',
    component: <RolesPage />,
  },

  // VIEW ROLES
  {
    path: '/view-roles/update/:id',
    component: <UpdateViewRolesPage />,
  },
  {
    path: '/view-roles/create',
    component: <CreateViewRolesPage />,
  },
  {
    path: '/view-roles',
    component: <ViewRolesPage />,
  },
  // VIEW
  {
    path: '/views/update/:id',
    component: <UpdateViewPage />,
  },
  {
    path: '/views/create',
    component: <CreateViewPage />,
  },
  {
    path: '/views',
    component: <ViewPage />,
  },
  // APIs
  {
    path: '/apis/create',
    component: <CreateAPIsPage />,
  },
  {
    path: '/apis/update/:id',
    component: <UpdateAPIsPage />,
  },
  {
    path: '/apis',
    component: <APIsPage />,
  },
  // API Roles
  {
    path: '/api-roles/update/:id',
    component: <UpdateAPIRolePage />,
  },
  {
    path: '/api-roles/create',
    component: <CreateAPIRolePage />,
  },
  {
    path: '/api-roles',
    component: <APIRolesPage />,
  },
  // Node key
  {
    path: '/node-key',
    component: <NodeKeyPage />,
  },

  // Key management
  {
    path: '/key-management',
    component: <KeyManagementPage />,
  },

  // Dashboard
  {
    path: '/dashboard',
    component: <DashboardPage />,
  },
  // DashboardAdmin
  {
    path: '/dashboard-admin',
    component: <DashboardAdminPage />,
  },
];
