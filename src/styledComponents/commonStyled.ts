import styled from 'styled-components';

export const ContentBoxShadow = styled.div`
  margin-top: 12px;
  background-color: #fff;
  padding: 12px;
  box-shadow: ${({ theme }) => theme.shadowContainer};
  margin-bottom: 12px;
`;
