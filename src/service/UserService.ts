import apiClient from './apiClient';

export const UserService = {
  getAllUsersService(page: number, size: number) {
    return apiClient.request({
      method: 'get',
      url: `/admin-api/account/list?page=${page}&size=${size}`,
    });
  },

  postUserService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/account/create',
    });
  },

  getDetailUserService(email: string) {
    return apiClient.request({
      method: 'get',
      url: '/admin-api/account/list?email=' + email,
    });
  },

  deleteUserService(email: any) {
    const data = { email };
    return apiClient.request({
      method: 'post',
      data,

      url: '/admin-api/account/delete',
    });
  },

  updateUserService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/account/update',
    });
  },

  changePasswordUserService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/account/change-acc-password',
    });
  },
};
