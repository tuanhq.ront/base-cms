import apiClient from './apiClient';

export const APIRolesService = {
  getAllAPIRolesServiceNotPagination() {
    return apiClient.request({
      method: 'GET',
      url: `/admin-api/api-role/list`,
    });
  },

  getAllAPIRolesService(page: number, size: number) {
    return apiClient.request({
      method: 'GET',
      url: `/admin-api/api-role/list?page=${page}&size=${size}`,
    });
  },

  postAPIRoleService(data: any) {
    return apiClient.request({
      method: 'POST',
      data,
      url: '/admin-api/api-role/create',
    });
  },

  updateAPIRoleService(data: any) {
    return apiClient.request({
      method: 'POST',
      data,
      url: '/admin-api/api-role/update',
    });
  },

  deleteAPIRoleService(id: string) {
    const data = { id };
    return apiClient.request({
      method: 'POST',
      data,
      url: '/admin-api/api-role/delete',
    });
  },

  getDetailAPIRoleService(id: string) {
    return apiClient.request({
      method: 'get',
      url: '/admin-api/api-role/list?id=' + id,
    });
  },
};
