import apiClient from './apiClient';

export const OrganizationService = {
  getAllOrganizationsServiceNotPagination() {
    return apiClient.request({
      method: 'get',
      url: `/admin-api/organization/list`,
    });
  },

  getAllOrganizationsService(page: number, size: number) {
    return apiClient.request({
      method: 'get',
      url: `/admin-api/organization/list?page=${page}&size=${size}`,
    });
  },

  postOrganizationsService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: `/admin-api/organization/create`,
    });
  },

  getDetailOrganizationsService(id: string) {
    return apiClient.request({
      method: 'get',
      url: '/admin-api/organization/list?id=' + id,
    });
  },

  updateOrganizationService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/organization/update',
    });
  },

  deleteOrganizationService(id: string) {
    const data = { id };
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/organization/delete',
    });
  },
};
