import apiClient from './apiClient';

export const ViewRolesService = {
  getAllViewRolesServiceNotPagination() {
    return apiClient.request({
      method: 'get',
      url: `/admin-api/view-role/list`,
    });
  },
  getAllViewRolesService(page: number, size: number) {
    return apiClient.request({
      method: 'get',
      url: `/admin-api/view-role/list?page=${page}&size=${size}`,
    });
  },

  postViewRolesService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/view-role/create',
    });
  },

  updateViewRolesService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/view-role/update',
    });
  },

  getDetailViewRolesService(id: string) {
    return apiClient.request({
      method: 'get',
      url: '/admin-api/view-role/list?id=' + id,
    });
  },

  deleteViewRolesService(id: string) {
    const data = { id };
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/view-role/delete',
    });
  },
};
