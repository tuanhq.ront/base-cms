import apiClient from './apiClient';

export interface ParamKey {
  public_key: string;
  organization_id: string;
  organization_name: string;
}

export const KeyManagementServices = {
  getListKeys(page: number, size: number) {
    return apiClient.request({
      method: 'get',
      url: `/admin-api/node-key/list?page=${page}&size=${size}`,
    });
  },

  // set key -> approve
  setKey(publicKey: string) {
    const data = {
      key: publicKey,
    };

    return apiClient.request({
      method: 'post',
      url: '/glx-api-smartcontract/set-key',
      data,
    });
  },

  approveKey(data: ParamKey) {
    return apiClient.request({
      method: 'post',
      url: `/admin-api/node-key/approve`,
      data,
    });
  },

  rejectKey(data: ParamKey) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/node-key/reject',
    });
  },

  deleteKey(data: ParamKey) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/node-key/delete',
    });
  },

  // delete-smart contract -> revoke

  deleteKeySmartContract(publicKey: string) {
    const data = {
      key: publicKey,
    };

    return apiClient.request({
      method: 'post',
      url: '/glx-api-smartcontract/delete-key',
      data,
    });
  },

  revokeKey(data: ParamKey) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/node-key/revoke',
    });
  },
};
