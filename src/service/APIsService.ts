import apiClient from './apiClient';

export const APIsService = {
  getAllAPIsServiceNotPagination() {
    return apiClient.request({
      method: 'GET',
      url: `/admin-api/api/list`,
    });
  },

  getAllAPIsService(page: number, size: number) {
    return apiClient.request({
      method: 'GET',
      url: `/admin-api/api/list?page=${page}&size=${size}`,
    });
  },

  getDetailAPIsService(id: string) {
    return apiClient.request({
      method: 'GET',
      url: '/admin-api/api/list?id=' + id,
    });
  },

  postAPIService(data: any) {
    return apiClient.request({
      method: 'POST',
      data,
      url: '/admin-api/api/create',
    });
  },

  updateAPIService(data: any) {
    return apiClient.request({
      method: 'POST',
      data,
      url: '/admin-api/api/update',
    });
  },

  deleteAPIService(id: string) {
    const data = { id };
    return apiClient.request({
      method: 'POST',
      data,
      url: '/admin-api/api/delete',
    });
  },
};
