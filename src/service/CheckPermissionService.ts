import { LoginService } from './LoginService';

export const CheckPermissionService = {
  getDataView(path: string) {
    const data = LoginService.getDataLocalStorage();
    if (data) {
      const permission = data.permission.find((per: any) => per.path === path);

      if (!permission) return null;
      return permission;
    }
    return null;
  },

  getAction(keyAction: string, path: string): string | null {
    const data = LoginService.getDataLocalStorage();
    if (!data) {
      return null;
    }

    const permission = data.permission.find((per: any) => per.path === path);
    if (!permission) return null;

    const actions = permission.actions;

    if (!actions || actions.length === 0) return null;

    const rs = actions.find((action: any) => action.key === keyAction);

    if (!rs) return null;

    return rs.title;
  },
};
