import apiClient from './apiClient';
import { LOCAL_STORAGE } from './constants';

export const LoginService = {
  loginAPI(email: string, password: string) {
    const param = { email, password };
    return apiClient.request({
      method: 'post',
      data: param,
      url: '/acc-svc/users/auth/login',
    });
  },

  logoutAPI() {
    const dataAccount = LoginService.getDataLocalStorage();

    if (dataAccount) {
      localStorage.removeItem(LOCAL_STORAGE);
    }

    return apiClient.request({
      headers: { 'X-USER-NAME': dataAccount.email },
      method: 'post',
      data: {
        email: dataAccount.email,
      },
      url: '/acc-svc/users/auth/logout',
    });
  },

  setDataLocalStorage(data: any) {
    localStorage.setItem(LOCAL_STORAGE, JSON.stringify(data));
  },

  getDataLocalStorage() {
    const data = localStorage.getItem(LOCAL_STORAGE);
    if (data) return JSON.parse(data);
    return null;
  },

  changePasswordAccount(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/account/change-password-self',
    });
  },

  logoutServices() {},
};
