import apiClient from './apiClient';

export const ViewsService = {
  getAllViewsServiceNotPagination() {
    return apiClient.request({
      method: 'get',
      url: `/admin-api/permission/list`,
    });
  },
  getAllViewsService(page: number, size: number) {
    return apiClient.request({
      method: 'get',
      url: `/admin-api/permission/list?page=${page}&size=${size}`,
    });
  },

  getDetailView(id: string) {
    return apiClient.request({
      method: 'get',
      url: '/admin-api/permission/list?id=' + id,
    });
  },

  postViewService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/permission/create',
    });
  },

  updateViewService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/permission/update',
    });
  },

  deleteViewService(id: string) {
    const data = { id };
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/permission/delete',
    });
  },

  // ACTIONS

  createActionForPermission(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/action/create',
    });
  },

  updateActionForPermission(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/action/update',
    });
  },

  deletesActionService(action_id: string, permission_id: string) {
    const data = { id: action_id, permission_id: permission_id };
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/action/delete',
    });
  },
};
