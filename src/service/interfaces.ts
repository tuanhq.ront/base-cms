export interface DataViewTypes {
  id: string;
  icon: string;
  actions: any[];
  path: string;
  title: string;
}

export const initialStateDataView: DataViewTypes = {
  id: '',
  icon: '',
  actions: [],
  path: '',
  title: '',
};
