import apiClient from './apiClient';
import { LoginService } from './LoginService';

export const NodeKeyServices = {
  uploadKey(data: any) {
    return apiClient.request({
      method: 'post',
      data: data,
      url: '/admin-api/node-key/upload',
    });
  },

  getNodeKeysOfOrganization(page: number, size: number) {
    const dataAccount = LoginService.getDataLocalStorage();
    const organization_id = dataAccount.organization_id;
    return apiClient.request({
      method: 'get',
      url: `/admin-api/node-key/list-organization?page=${page}&size=${size}&organization_id=${organization_id}`,
    });
  },
};
