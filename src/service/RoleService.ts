import apiClient from './apiClient';

export const RoleService = {
  getAllRoleService() {
    return apiClient.request({
      method: 'get',
      url: '/admin-api/role/list',
    });
  },

  postRoleService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/role/create',
    });
  },

  updateRoleService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/role/update',
    });
  },

  getDetailRoleService(id: string) {
    return apiClient.request({
      method: 'get',
      url: '/admin-api/role/list?id=' + id,
    });
  },

  deleteRoleService(id: string) {
    const data = { id };
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/role/delete',
    });
  },
};
