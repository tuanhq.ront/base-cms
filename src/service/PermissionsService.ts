import apiClient from './apiClient';

export const PermissionsService = {
  getAllPermissionsService() {
    return apiClient.request({
      method: 'get',
      url: '/admin-api/permission/list',
    });
  },

  getDetailPermission(id: string) {
    return apiClient.request({
      method: 'get',
      url: '/admin-api/permission/list?id=' + id,
    });
  },

  postPermissionService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/permission/create',
    });
  },

  updatePermissionService(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/permission/update',
    });
  },

  deletePermissionService(id: string) {
    const data = { id };
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/permission/delete',
    });
  },

  // ACTIONS

  createActionForPermission(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/action/create',
    });
  },

  updateActionForPermission(data: any) {
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/action/update',
    });
  },

  deletesActionService(id: string) {
    const data = { id };
    return apiClient.request({
      method: 'post',
      data,
      url: '/admin-api/action/delete',
    });
  },
};
