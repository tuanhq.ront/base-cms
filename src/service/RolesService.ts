import apiClient from './apiClient';

export const RolesService = {
  getAllRoleServiceNotPagination() {
    return apiClient.request({
      method: 'GET',
      url: '/admin-api/role/list',
    });
  },

  getAllRoleService(page: number, size: number) {
    return apiClient.request({
      method: 'GET',
      url: `/admin-api/role/list?page=${page}&size=${size}`,
    });
  },

  postRoleService(data: any) {
    return apiClient.request({
      method: 'POST',
      data,
      url: '/admin-api/role/create',
    });
  },

  updateRoleService(data: any) {
    return apiClient.request({
      method: 'POST',
      data,
      url: '/admin-api/role/update',
    });
  },

  getDetailRoleService(id: string) {
    return apiClient.request({
      method: 'GET',
      url: '/admin-api/role/list?id=' + id,
    });
  },

  deleteRoleService(id: string) {
    const data = { id };
    return apiClient.request({
      method: 'POST',
      data,
      url: '/admin-api/role/delete',
    });
  },
};
