import { Button, Popconfirm, Space, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import Loading from 'components/Loading';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';
import { KeyManagementServices, ParamKey } from 'service/KeyManagementServices';
import { ContentBoxShadow } from 'styledComponents/commonStyled';

function KeyManagementPage() {
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);

  const [keys, setKeys] = useState([]);

  const [loadingGetAll, setLoadingGetAll] = useState(false);
  const [loadingApprove, setLoadingApprove] = useState(false);

  // CHECKING PERMISSION
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, location.pathname);
  };

  const handleGetListKey = () => {
    setLoadingGetAll(true);
    KeyManagementServices.getListKeys(page, size)
      .then((res: any) => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setKeys(res.data.rows);
        setTotal(res.data.total);
      })
      .catch((err: any) => {
        console.error(err);
        NotificationError('Error', 'Get list keys failed');
      })
      .finally(() => {
        setLoadingGetAll(false);
      });
  };

  const handleSetKey = (nodeKey: any) => {
    setLoadingApprove(true);
    KeyManagementServices.setKey(nodeKey.public_key)
      .then((res: any) => {
        if (!res.data.success) {
          NotificationError('Error', res.data.payload);
          return;
        }

        const params: ParamKey = {
          public_key: nodeKey.public_key,
          organization_id: nodeKey.organization_id,
          organization_name: nodeKey.organization_name,
        };

        return KeyManagementServices.approveKey(params);
      })
      .then((res: any) => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Approve key success');
        handleGetListKey();
      })
      .catch((err: any) => {
        console.error(err);
        NotificationError('Error', 'Approve key failed');
      })
      .finally(() => {
        setLoadingApprove(false);
      });
  };
  //
  const handleRevoke = (nodeKey: any) => {
    setLoadingApprove(true);

    KeyManagementServices.deleteKeySmartContract(nodeKey.public_key)
      .then((res: any) => {
        if (!res.data.success) {
          NotificationError('Error', res.data.payload);
          return;
        }

        const params: ParamKey = {
          public_key: nodeKey.public_key,
          organization_id: nodeKey.organization_id,
          organization_name: nodeKey.organization_name,
        };

        return KeyManagementServices.revokeKey(params);
      })
      .then((res: any) => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Revoke key success');
        handleGetListKey();
      })
      .catch((err: any) => {
        console.error(err);
        NotificationError('Error', 'Revoke key failed');
      })
      .finally(() => {
        setLoadingApprove(false);
      });
  };

  const handleReject = (nodeKey: any) => {
    const params: ParamKey = {
      public_key: nodeKey.public_key,
      organization_id: nodeKey.organization_id,
      organization_name: nodeKey.organization_name,
    };
    KeyManagementServices.rejectKey(params)
      .then((res: any) => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Reject key success');
        handleGetListKey();
      })
      .catch((err: any) => {
        console.error(err);
        NotificationError('Error', 'Reject key failed');
      });
  };

  const handleDeleteNodeKey = (nodeKey: any) => {
    const params: ParamKey = {
      public_key: nodeKey.public_key,
      organization_id: nodeKey.organization_id,
      organization_name: nodeKey.organization_name,
    };

    KeyManagementServices.deleteKey(params)
      .then((res: any) => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }
        NotificationSuccess('Success', 'Delete key success');
        handleGetListKey();
      })
      .catch((err: any) => {
        console.error(err);
        NotificationError('Error', 'Delete key failed');
      });
  };

  useEffect(() => {
    handleGetListKey();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, size]);

  const columns: ColumnsType<any> = [
    {
      key: 'stt',
      title: '#',
      width: 60,
      render: (text, record, index) => index + 1,
    },
    {
      key: 'organization_name',
      title: 'Organization',
      dataIndex: 'organization_name',
    },
    {
      key: 'public_key',
      title: 'Public Key',
      dataIndex: 'public_key',
      ellipsis: true,
    },
    {
      key: 'create_at',
      title: 'Created At',
      dataIndex: 'create_at',
      width: 200,
      render: (text: any) => {
        return (
          <>{moment(new Date(parseInt(text))).format('DD/MM/YYYY HH:mm:ss')}</>
        );
      },
    },

    {
      key: 'key_status',
      title: 'Status',
      dataIndex: 'key_status',
      width: 160,
      render: (text: any) => (
        <div className={`keyTag keyTag-${text}`}>
          {text === 0 && 'Pending'}
          {text === 1 && 'Approve'}
          {text === 2 && 'Reject'}
          {text === 3 && 'Revoke'}
        </div>
      ),
    },
    {
      key: 'action',
      title: 'Action',
      width: 300,
      render: (text: any, record: any) => (
        <Space>
          {record.key_status === 0 && (
            <>
              {handleGetAction('approve') && (
                <Popconfirm
                  title="Are you sure to approve this key?"
                  onConfirm={() => handleSetKey(record)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button size="small" type="primary">
                    Approve
                  </Button>
                </Popconfirm>
              )}

              {handleGetAction('reject') && (
                <Popconfirm
                  title="Are you sure reject this key?"
                  onConfirm={() => handleReject(record)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button size="small" type="primary" danger>
                    Reject
                  </Button>
                </Popconfirm>
              )}
            </>
          )}

          {record.key_status === 2 && (
            <>
              {handleGetAction('delete') && (
                <Popconfirm
                  title="Are you sure delete this key?"
                  onConfirm={() => handleDeleteNodeKey(record)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button size="small" type="default" danger>
                    Delete
                  </Button>
                </Popconfirm>
              )}
            </>
          )}

          {record.key_status === 1 && (
            <>
              {handleGetAction('revoke') && (
                <Popconfirm
                  title="Are you sure revoke this key?"
                  onConfirm={() => handleRevoke(record)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button size="small" type="default" danger>
                    Revoke
                  </Button>
                </Popconfirm>
              )}
            </>
          )}
        </Space>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />

      <ContentBoxShadow>
        <Table
          rowKey={'public_key'}
          size="small"
          columns={columns}
          dataSource={keys}
          loading={loadingGetAll}
          pagination={{
            onChange: (page: any, size: any) => {
              setPage(page);
              setSize(size);
            },
            showSizeChanger: true,
            pageSizeOptions: [10, 20, 30],
            current: page,
            pageSize: size,
            total: total,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} of ${total} items`,
          }}
        />
      </ContentBoxShadow>

      {loadingApprove && <Loading />}
    </LayoutDashboard>
  );
}

export default KeyManagementPage;
