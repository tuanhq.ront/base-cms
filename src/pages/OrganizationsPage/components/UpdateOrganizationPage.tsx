import { Button, Form, Input, Popconfirm, Skeleton, Space } from 'antd';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { OrganizationService } from 'service/OrganizationService';
import styled from 'styled-components';
import { ContentBoxShadow } from 'styledComponents/commonStyled';
import { OrganizationTypes } from '../interfaces';

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

function UpdateOrganizationPage() {
  const { id } = useParams() as { id: string };
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const [organization, setOrganization] = useState<OrganizationTypes>({
    name: '',
  });
  const [nodes, setNodes] = useState<any>([]);

  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [loadingGetDetail, setLoadingGetDetail] = useState(false);

  // CHECKING PERMISSION
  const path = '/' + useLocation().pathname.split('/')[1];

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, path);
  };
  //

  const onFinish = (values: any) => {
    // setLoadingUpdate(true);
    const param = { id, ...values };

    setLoadingUpdate(true);
    OrganizationService.updateOrganizationService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Update Organization Success');
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Update organization failed');
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  const handleCancel = () => {
    navigate(-1);
  };

  const handleGetDetailOrganization = () => {
    setLoadingGetDetail(true);
    OrganizationService.getDetailOrganizationsService(id)
      .then(res => {
        if (res.data.code !== 0) {
          return;
        }

        const data = res.data.rows[0];
        setOrganization(data);
        setNodes(res.data.rows[0].node);
      })
      .catch(err => {
        console.log(err);
        NotificationError('Error', '');
      })
      .finally(() => {
        setLoadingGetDetail(false);
      });
  };

  useEffect(() => {
    handleGetDetailOrganization();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [organization]);

  // const columns: any = [
  //   {
  //     key: '#',
  //     title: '#',
  //     width: 40,
  //     align: 'center',
  //     render: (text: any, record: any, index: any) => <>{index + 1}</>,
  //   },
  //   {
  //     key: 'name',
  //     title: 'Name',
  //     dataIndex: 'name',
  //   },
  //   {
  //     key: 'organization_owner',
  //     title: 'Organization Owner',
  //     dataIndex: 'organization_owner',
  //   },
  //   {
  //     key: `action`,
  //     width: 200,
  //     title: 'Action',
  //     align: 'center',
  //     render: (text: any, record: any) => (
  //       <Space>
  //         <Button size="small" type="primary">
  //           Update
  //         </Button>

  //         <Popconfirm
  //           placement="top"
  //           title="Are you sure to delete this action?"
  //           okText="Yes"
  //           cancelText="No"
  //         >
  //           <Button size="small" type="primary" danger>
  //             Delete
  //           </Button>
  //         </Popconfirm>
  //       </Space>
  //     ),
  //   },
  // ];
  return (
    <LayoutDashboard>
      <HelmetComponent title="Update Organization" />
      <TitlePage title="Update Organization" />

      <Wrapper>
        <ContentBoxShadow>
          {loadingGetDetail ? (
            <Skeleton />
          ) : (
            <Form
              {...layout}
              form={form}
              onFinish={onFinish}
              className="updateOrganization-info-form"
            >
              <Form.Item
                name="name"
                label="Name"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={organization.name}
              >
                <Input />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                <Space>
                  {handleGetAction('update') && (
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={loadingUpdate}
                    >
                      Update
                    </Button>
                  )}

                  <Button type="default" onClick={handleCancel}>
                    Cancel
                  </Button>
                </Space>
              </Form.Item>
            </Form>
          )}
        </ContentBoxShadow>

        {/* <div className="updateOrganization-nodes">
          <div className="updateOrganization-nodes-title">Nodes</div>

          <Button type="primary" className="updateOrganization-nodes-button">
            Add Node
          </Button>

          <Table
            rowKey="id"
            size="small"
            columns={columns}
            dataSource={nodes}
            loading={loadingGetDetail}
            pagination={{
              onChange: page => {},
              pageSize: 10,
              total: 5,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} nodes`,
            }}
          />
        </div> */}
      </Wrapper>
    </LayoutDashboard>
  );
}

export default UpdateOrganizationPage;
const Wrapper = styled.div`
  .updateOrganization {
    &-info {
      &-form {
        max-width: 600px;
      }
    }

    &-nodes {
      background-color: #fff;
      padding: 12px;
      box-shadow: ${({ theme }) => theme.shadowContainer};

      &-title {
        font-size: 18px;
        margin-bottom: 8px;
        font-weight: 500;
      }

      &-button {
        margin-bottom: 10px;
      }
    }
  }
`;
