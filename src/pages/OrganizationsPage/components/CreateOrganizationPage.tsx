import { Button, Form, Input, Popconfirm, Space } from 'antd';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { OrganizationService } from 'service/OrganizationService';
import styled from 'styled-components';
import { ContentBoxShadow } from 'styledComponents/commonStyled';

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

function CreateOrganizationPage() {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [loadingCreate, setLoadingCreate] = useState(false);

  const onFinish = (values: any) => {
    setLoadingCreate(true);
    const param = { ...values };

    OrganizationService.postOrganizationsService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Create organization success');
        navigate('/organizations');
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Create organizations failed');
      })
      .finally(() => {
        setLoadingCreate(false);
      });
  };

  const handleCancel = () => {
    navigate(-1);
  };

  // const columns: any = [
  //   {
  //     key: '#',
  //     title: '#',
  //     width: 40,
  //     align: 'center',
  //     render: (text: any, record: any, index: any) => <>{index + 1}</>,
  //   },
  //   {
  //     key: 'title',
  //     title: 'Title',
  //     dataIndex: 'title',
  //   },
  //   {
  //     key: `action`,
  //     width: 200,
  //     title: 'Action',
  //     align: 'center',
  //     render: (text: any, record: any) => (
  //       <Space>
  //         <Button size="small" type="primary">
  //           Update
  //         </Button>

  //         <Popconfirm
  //           placement="top"
  //           title="Are you sure to delete this action?"
  //           okText="Yes"
  //           cancelText="No"
  //         >
  //           <Button size="small" type="primary" danger>
  //             Delete
  //           </Button>
  //         </Popconfirm>
  //       </Space>
  //     ),
  //   },
  // ];

  return (
    <LayoutDashboard>
      <HelmetComponent title="Create Organization" />
      <TitlePage title="Create Organization" />

      <Wrapper>
        <ContentBoxShadow>
          <Form
            {...layout}
            form={form}
            onFinish={onFinish}
            className="createOrganization-info-form"
          >
            <Form.Item
              name="name"
              label="Name"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loadingCreate}
                >
                  Create
                </Button>
                <Button type="default" onClick={handleCancel}>
                  Cancel
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </ContentBoxShadow>

        {/* <div className="createOrganization-nodes">
          <div className="createOrganization-nodes-title">Nodes</div>

          <Button type="primary" className="createOrganization-nodes-button">
            Add Node
          </Button>

          <Table
            rowKey="id"
            size="small"
            columns={columns}
            dataSource={[]}
            pagination={{
              onChange: page => {},
              pageSize: 10,
              total: 5,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} nodes`,
            }}
          />
        </div> */}
      </Wrapper>
    </LayoutDashboard>
  );
}

export default CreateOrganizationPage;

const Wrapper = styled.div`
  .createOrganization {
    &-info {
      &-form {
        max-width: 600px;
      }
    }

    &-nodes {
      background-color: #fff;
      padding: 12px;
      box-shadow: ${({ theme }) => theme.shadowContainer};

      &-title {
        font-size: 18px;
        margin-bottom: 8px;
        font-weight: 500;
      }

      &-button {
        margin-bottom: 10px;
      }
    }
  }
`;
