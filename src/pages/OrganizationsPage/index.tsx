import { PlusOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Space, Table, Tag } from 'antd';
import { ColumnsType } from 'antd/es/table';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';
import { OrganizationService } from 'service/OrganizationService';
import styled from 'styled-components';
import { ContentBoxShadow } from 'styledComponents/commonStyled';

function OrganizationsPage() {
  const navigate = useNavigate();

  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);

  const [organizations, setOrganizations] = useState([]);
  const [loadingGetAll, setLoadingGetAll] = useState(false);

  // CHECKING PERMISSION
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, location.pathname);
  };
  //

  const handleCreateOrganization = () => {
    navigate('/organizations/create');
  };

  const handleUpdateCreateOrganization = (id: string) => {
    navigate('/organizations/update/' + id);
  };

  const handleOrganization = (id: string) => {
    OrganizationService.deleteOrganizationService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Delete organization success');
        handleGetAllOrganizations();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Delete organization failed');
      })
      .finally(() => {});
  };

  const handleGetAllOrganizations = () => {
    setLoadingGetAll(true);
    OrganizationService.getAllOrganizationsService(page, size)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setOrganizations(res.data.rows);
        setTotal(res.data.total);
      })
      .catch(err => {
        NotificationError('Error', 'Get list organizations failed');
        console.error(err);
      })
      .finally(() => {
        setLoadingGetAll(false);
      });
  };

  useEffect(() => {
    handleGetAllOrganizations();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, size]);

  const columns: ColumnsType<any> = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
      width: 300,
    },
    {
      key: 'node',
      title: 'Node',
      dataIndex: 'node',
      render: (text: any, record: any) => (
        <>
          {text.map((node: any, index: any) => (
            <Tag color="blue" key={index}>
              {node.name}
            </Tag>
          ))}
        </>
      ),
    },
    {
      key: `action`,
      title: 'Action',
      align: 'center',
      width: 200,

      render: (text: any, record: any) => (
        <Space>
          <Button
            size="small"
            type="primary"
            onClick={() => handleUpdateCreateOrganization(record.id)}
          >
            Detail
          </Button>

          {handleGetAction('delete') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this organization?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleOrganization(record.id)}
            >
              <Button size="small" type="primary" danger>
                Delete
              </Button>
            </Popconfirm>
          )}
        </Space>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />

      <Wrapper>
        {handleGetAction('create') && (
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleCreateOrganization}
          >
            Create
          </Button>
        )}

        <ContentBoxShadow>
          <Table
            rowKey={'id'}
            size="small"
            columns={columns}
            dataSource={organizations}
            loading={loadingGetAll}
            pagination={{
              onChange: (page: any, size: any) => {
                setPage(page);
                setSize(size);
              },
              showSizeChanger: true,
              pageSizeOptions: [10, 20, 30],
              current: page,
              pageSize: size,
              total: total,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} organizations`,
            }}
          />
        </ContentBoxShadow>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default OrganizationsPage;
const Wrapper = styled.div`
  .organizationPage {
    &-containerTagsRole {
      display: flex;
      justify-content: flex-start;
      align-items: center;
    }

    &-tagsRole {
      border: 1px solid #ccc;
      padding: 0px 5px;
      margin-right: 4px;
      border-radius: 4px;
    }
  }
`;
