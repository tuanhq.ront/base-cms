export interface ViewRolesTypes {
  name: string;
  description: string;
  permissions: any[];
  actions: any[];
}
