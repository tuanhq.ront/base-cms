import { Button, Col, Form, Input, Row, Skeleton, Space, Tree } from 'antd';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { ViewRolesService } from 'service/ViewRolesService';
import styled from 'styled-components';
import HelmetComponent from '../../../components/HelmetComponent';
import LayoutDashboard from '../../../components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from '../../../components/Notification';
import TitlePage from '../../../components/TitlePage';
import { ViewsService } from '../../../service/ViewsService';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

function CreateViewRolesPage() {
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const [views, setViews] = useState<any[]>([]);

  const [loadingCreate, setLoadingCreate] = useState(false);
  const [loadingGetViews, setLoadingGetViews] = useState(false);

  const [permissionsChecked, setPermissionsChecked] = useState([]);
  const [listChecked, setListChecked] = useState([]);

  const onFinish = (values: any) => {
    const param = {
      ...values,
      permissions: permissionsChecked,
    };

    setLoadingCreate(true);
    ViewRolesService.postViewRolesService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Create view roles success');
        navigate(-1);
        return;
      })
      .catch(err => {
        NotificationError('Error', 'Create view role failed');
      })
      .finally(() => {
        setLoadingCreate(false);
      });
  };

  const handleCancel = () => {
    navigate(-1);
  };

  const onCheck = (checkedKeysValue: any, e: any) => {
    const listAction = e.checkedNodes.filter(node => node.type === 'action');

    const listView = e.checkedNodes
      .filter(node => node.type === 'view')
      .map(node => ({
        index: node.index,
        permission_id: node.id,
        actions: [
          ...listAction
            .filter(action => action.parent_id === node.id)
            .map(action => action.id),
        ],
      }));

    setPermissionsChecked(listView);

    const newListChecked = listView
      .map(node => [node.permission_id, ...node.actions])
      .flat();

    setListChecked(newListChecked);
  };

  const handleGetAllViews = () => {
    setLoadingGetViews(false);
    ViewsService.getAllViewsServiceNotPagination()
      .then(res => {
        if (res.data.code !== 0) {
          return;
        }

        const data = res.data.rows;
        const newViews = data.map((e: any) => ({
          ...e,
          key: e.id,
          type: 'view',
          children: e.actions.map((a: any) => ({
            ...a,
            key: a.id,
            type: 'action',
            parent_id: e.id,
          })),
        }));

        setViews(newViews);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        setLoadingGetViews(false);
      });
  };

  useEffect(() => {
    handleGetAllViews();
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [views]);

  return (
    <LayoutDashboard>
      <HelmetComponent title="Create View Role" />
      <TitlePage title="Create View Role" />

      <Wrapper>
        <Form {...layout} form={form} onFinish={onFinish}>
          <Row gutter={[8, 8]}>
            <Col lg={15}>
              <Form.Item
                name="name"
                label="Name"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="description"
                label="Description"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
              >
                <Input.TextArea rows={4} />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
                <Space>
                  <Button
                    type="primary"
                    htmlType="submit"
                    loading={loadingCreate}
                  >
                    Create
                  </Button>
                  <Button type="default" onClick={handleCancel}>
                    Cancel
                  </Button>
                </Space>
              </Form.Item>
            </Col>

            <Col lg={9}>
              <Form.Item
                label="Views"
                name="permissions"
                labelAlign="left"
                // rules={[{ required: true, message: 'This field is require!' }]}
              >
                {/* <Select allowClear mode="multiple" showArrow>
              <Option value="permissions1">permissions1</Option>
              <Option value="permissions2">permissions2</Option>
            </Select> */}

                {loadingGetViews ? (
                  <Skeleton />
                ) : (
                  <Tree
                    treeData={views}
                    checkable
                    onCheck={onCheck}
                    checkStrictly={true}
                    defaultExpandAll={true}
                    checkedKeys={listChecked}
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default CreateViewRolesPage;

const Wrapper = styled.div`
  background-color: #fff;
  padding: 12px;
  box-shadow: ${({ theme }) => theme.shadowContainer};
  margin-bottom: 12px;
`;
