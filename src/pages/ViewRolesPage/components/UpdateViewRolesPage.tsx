import { Button, Col, Form, Input, Row, Skeleton, Space, Tree } from 'antd';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { ViewRolesService } from 'service/ViewRolesService';
import styled from 'styled-components';
import HelmetComponent from '../../../components/HelmetComponent';
import LayoutDashboard from '../../../components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from '../../../components/Notification';
import TitlePage from '../../../components/TitlePage';
import { ViewsService } from '../../../service/ViewsService';
import { ViewRolesTypes } from '../interfaces';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

function UpdateViewRolesPage() {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const { id } = useParams() as { id: string };

  const [role, setRole] = useState<ViewRolesTypes>({
    name: '',
    description: '',
    permissions: [],
    actions: [],
  });

  const [views, setViews] = useState<any[]>([]);

  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [loadingGetDetail, setLoadingGetDetail] = useState(false);

  const [permissionsChecked, setPermissionsChecked] = useState([]);
  const [listChecked, setListChecked] = useState([]);

  // CHECKING PERMISSION
  const path = '/' + useLocation().pathname.split('/')[1];

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, path);
  };
  //

  const onFinish = (values: any) => {
    const param = {
      ...values,
      permissions: permissionsChecked,
      id,
    };

    setLoadingUpdate(true);

    ViewRolesService.updateViewRolesService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Update View Roles Success');
        handleGetDetailViewRoles();
        handleCancel();
        return;
      })
      .catch(err => {
        NotificationError('Error', 'Update view role failed');
        console.error(err);
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  const handleCancel = () => {
    navigate(-1);
  };

  const onCheck = (checkedKeysValue: any, e: any) => {
    const listAction = e.checkedNodes.filter(node => node.type === 'action');

    const listView = e.checkedNodes
      .filter(node => node.type === 'view')
      .map(node => ({
        index: node.index,
        permission_id: node.id,
        actions: [
          ...listAction
            .filter(action => action.parent_id === node.id)
            .map(action => action.id),
        ],
      }));

    setPermissionsChecked(listView);

    const newListChecked = listView
      .map(node => [node.permission_id, ...node.actions])
      .flat();

    setListChecked(newListChecked);
  };

  const handleGetAllViews = () => {
    ViewsService.getAllViewsServiceNotPagination().then(res => {
      if (res.data.code !== 0) {
        return;
      }

      const data = res.data.rows;

      const newViews = data.map((e: any) => ({
        ...e,
        key: e.id,
        type: 'view',
        children: e.actions.map((a: any) => ({
          ...a,
          key: a.id,
          type: 'action',
          parent_id: e.id,
        })),
      }));

      setViews(newViews);
    });
  };

  const handleGetDetailViewRoles = () => {
    setLoadingGetDetail(true);
    ViewRolesService.getDetailViewRolesService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setRole(res.data.rows[0]);

        // check state
        const newPermissionsChecked = res.data.rows[0].permissions.map(
          (permission: any) => ({
            index: permission.index,
            permission_id: permission.id,
            actions: permission.actions.map((action: any) => action.id),
          }),
        );

        setPermissionsChecked(newPermissionsChecked);

        // checked UI
        const newListChecked = res.data.rows[0].permissions
          .map(node => [node.id, ...node.actions.map((a: any) => a.id)])
          .flat();

        setListChecked(newListChecked);
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Get detail view role failed');
      })
      .finally(() => {
        setLoadingGetDetail(false);
      });
  };

  useEffect(() => {
    handleGetAllViews();

    handleGetDetailViewRoles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [role]);

  return (
    <LayoutDashboard>
      <HelmetComponent title="Update View Role" />
      <TitlePage title="Update View Role" />

      <Wrapper>
        {loadingGetDetail ? (
          <Skeleton />
        ) : (
          <Form {...layout} form={form} onFinish={onFinish}>
            <Row gutter={[8, 8]}>
              <Col lg={15}>
                <Form.Item
                  name="name"
                  label="Name"
                  labelAlign="left"
                  rules={[
                    { required: true, message: 'This field is require!' },
                  ]}
                  initialValue={role.name}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="description"
                  label="Description"
                  labelAlign="left"
                  rules={[
                    { required: true, message: 'This field is require!' },
                  ]}
                  initialValue={role.description}
                >
                  <Input.TextArea rows={4} />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
                  <Space>
                    {handleGetAction('update') && (
                      <Button
                        type="primary"
                        htmlType="submit"
                        loading={loadingUpdate}
                      >
                        Update
                      </Button>
                    )}

                    <Button type="default" onClick={handleCancel}>
                      Cancel
                    </Button>
                  </Space>
                </Form.Item>
              </Col>

              <Col lg={9}>
                <Form.Item
                  label="Views"
                  name="permissions"
                  labelAlign="left"
                  // rules={[{ required: true, message: 'This field is require!' }]}
                >
                  {/* <Select allowClear mode="multiple" showArrow>
              <Option value="permissions1">permissions1</Option>
              <Option value="permissions2">permissions2</Option>
            </Select> */}

                  <Tree
                    treeData={views}
                    checkable
                    onCheck={onCheck}
                    checkStrictly={true}
                    defaultExpandAll={true}
                    checkedKeys={listChecked}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        )}
      </Wrapper>
    </LayoutDashboard>
  );
}

export default UpdateViewRolesPage;

const Wrapper = styled.div`
  background-color: #fff;
  padding: 12px;
  box-shadow: ${({ theme }) => theme.shadowContainer};
  margin-bottom: 12px;
`;
