import { PlusOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Space, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';
import { ViewRolesService } from 'service/ViewRolesService';
import styled from 'styled-components';
import HelmetComponent from '../../components/HelmetComponent';
import LayoutDashboard from '../../components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from '../../components/Notification';
import TitlePage from '../../components/TitlePage';

function ViewRolesPage() {
  const navigate = useNavigate();

  const [viewRoles, setViewRoles] = useState<any[]>([]);

  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);

  const [loadingGetAll, setLoadingGetAll] = useState(false);

  // CHECKING PERMISSION
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  const handleCheckAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, location.pathname);
  };
  //

  const handleOpenCreateViewRole = () => {
    navigate('/view-roles/create');
  };

  const handleUpdateViewRole = (id: string) => {
    navigate('/view-roles/update/' + id);
  };

  const handleGetViewRoles = () => {
    setLoadingGetAll(true);
    ViewRolesService.getAllViewRolesService(page, size)
      .then(res => {
        if (res.data.code !== 0) {
          return;
        }

        setViewRoles(res.data.rows);
        setTotal(res.data.total);
      })
      .catch(err => {
        NotificationError('Error', 'Get list view roles failed');
      })
      .finally(() => {
        setLoadingGetAll(false);
      });
  };

  const handleDeleteViewRoles = (id: string) => {
    ViewRolesService.deleteViewRolesService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Delete view role success');
        handleGetViewRoles();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Delete view role failed');
      })
      .finally(() => {});
  };

  useEffect(() => {
    handleGetViewRoles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, size]);

  const columns: any = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },

    {
      key: `action`,
      title: 'Action',
      align: 'center',
      width: 200,
      render: (text: any, record: any) => (
        <Space>
          <Button
            size="small"
            type="primary"
            onClick={() => handleUpdateViewRole(record.id)}
          >
            Detail
          </Button>

          {handleCheckAction('delete') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this view role?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleDeleteViewRoles(record.id)}
            >
              <Button type="primary" size="small" danger>
                Delete
              </Button>
            </Popconfirm>
          )}
        </Space>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />

      {handleCheckAction('create') && (
        <Button
          type="primary"
          icon={<PlusOutlined />}
          onClick={handleOpenCreateViewRole}
        >
          Create
        </Button>
      )}

      <Wrapper>
        <div className="viewRolesPage-table">
          <Table
            rowKey="id"
            size="small"
            columns={columns}
            dataSource={viewRoles}
            loading={loadingGetAll}
            pagination={{
              onChange: (page: any, size: any) => {
                setPage(page);
                setSize(size);
              },
              showSizeChanger: true,
              pageSizeOptions: [10, 20, 30],
              current: page,
              pageSize: size,
              total: total,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} View Roles`,
            }}
          />
        </div>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default ViewRolesPage;

const Wrapper = styled.div`
  .viewRolesPage {
    &-table {
      background-color: #fff;
      padding: 12px;
      box-shadow: ${({ theme }) => theme.shadowContainer};
      margin-bottom: 12px;
      margin-top: 12px;
    }
  }
`;
