import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import React from 'react';

function EmptyPage() {
  return <LayoutDashboard>{}</LayoutDashboard>;
}

export default EmptyPage;
