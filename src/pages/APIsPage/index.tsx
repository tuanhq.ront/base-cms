import { PlusOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Space, Table } from 'antd';

import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { APIsService } from 'service/APIsService';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';

import styled from 'styled-components';
import HelmetComponent from '../../components/HelmetComponent';
import LayoutDashboard from '../../components/Layouts/LayoutDashboard';
import TitlePage from '../../components/TitlePage';

function APIsPage() {
  const navigate = useNavigate();

  const [loadingGetAll, setLoadingGetAll] = useState(false);
  const [APIs, setAPIs] = useState([]);

  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);

  // CHECKING PERMISSION
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  const handleCheckAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, location.pathname);
  };
  //

  const handleCreateAPI = () => {
    navigate('/apis/create');
  };

  const handleUpdateAPI = (id: string) => {
    navigate('/apis/update/' + id);
  };

  const handleDeleteAPI = (id: string) => {
    APIsService.deleteAPIService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Delete API success');
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Delete API failed');
      })
      .finally(() => {
        handleGetAllAPIs();
      });
  };

  const handleGetAllAPIs = () => {
    setLoadingGetAll(true);
    APIsService.getAllAPIsService(page, size)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setAPIs(res.data.rows);
        setTotal(res.data.total);
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Get list APIs failed');
      })
      .finally(() => {
        setLoadingGetAll(false);
      });
  };

  useEffect(() => {
    handleGetAllAPIs();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, size]);

  const columns: any = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },

    {
      key: 'method',
      title: 'Method',
      dataIndex: 'method',
    },

    {
      key: 'path',
      title: 'Path',
      dataIndex: 'path',
    },

    {
      key: `action`,
      title: 'Action',
      width: 200,
      align: 'center',
      render: (text: any, record: any) => (
        <Space>
          <Button
            size="small"
            type="primary"
            onClick={() => handleUpdateAPI(record.id)}
          >
            Detail
          </Button>

          {handleCheckAction('delete') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this API?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleDeleteAPI(record.id)}
            >
              <Button size="small" type="primary" danger>
                Delete
              </Button>
            </Popconfirm>
          )}
        </Space>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />

      <Wrapper>
        {handleCheckAction('create') && (
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleCreateAPI}
          >
            Create
          </Button>
        )}

        <div className="APIs-table">
          <Table
            rowKey={'id'}
            size="small"
            columns={columns}
            dataSource={APIs}
            loading={loadingGetAll}
            pagination={{
              onChange: (page: any, size: any) => {
                setPage(page);
                setSize(size);
              },
              showSizeChanger: true,
              pageSizeOptions: [10, 20, 30],
              current: page,
              pageSize: size,
              total: total,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} items`,
            }}
          />
        </div>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default APIsPage;
const Wrapper = styled.div`
  .APIs {
    &-table {
      margin-top: 12px;
      background-color: #fff;
      padding: 12px;
      box-shadow: ${({ theme }) => theme.shadowContainer};
      margin-bottom: 12px;
    }

    &-containerTagsRole {
      display: flex;
      justify-content: flex-start;
      align-items: center;
    }

    &-tagsRole {
      border: 1px solid #ccc;
      padding: 0px 5px;
      margin-right: 4px;
      border-radius: 4px;
    }
  }
`;
