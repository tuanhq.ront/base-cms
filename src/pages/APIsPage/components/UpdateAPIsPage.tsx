import { Button, Form, Input, Select, Skeleton, Space } from 'antd';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { APIsService } from 'service/APIsService';
import { CheckPermissionService } from 'service/CheckPermissionService';
import styled from 'styled-components';
import { APITypes } from '../interfaces';

const { Option } = Select;
const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

function UpdateAPIsPage() {
  const { id } = useParams() as { id: string };
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const [API, setAPI] = useState<APITypes>({ name: '', method: '', path: '' });

  const [loadingGetDetail, setLoadingGetDetail] = useState(false);
  const [loadingUpdate, setLoadingUpdate] = useState(false);

  // CHECKING PERMISSION
  const path = '/' + useLocation().pathname.split('/')[1];

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, path);
  };
  //

  const handleFinish = (values: any) => {
    const param = {
      ...values,
      id,
    };

    setLoadingUpdate(true);
    APIsService.updateAPIService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Update API Success');
        handleGetDetailAPI();
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Update API failed');
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  const handleCancel = () => {
    navigate(-1);
  };

  const handleGetDetailAPI = () => {
    setLoadingGetDetail(true);
    APIsService.getDetailAPIsService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setAPI(res.data.rows[0]);
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Get detail API failed');
      })
      .finally(() => {
        setLoadingGetDetail(false);
      });
  };

  useEffect(() => {
    handleGetDetailAPI();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [API]);

  return (
    <LayoutDashboard>
      <HelmetComponent title="Update API" />
      <TitlePage title="Update API" />

      <Wrapper>
        <div className="createAPI">
          {loadingGetDetail ? (
            <Skeleton />
          ) : (
            <Form
              {...layout}
              form={form}
              className="createAPI-form"
              onFinish={handleFinish}
            >
              <Form.Item
                name="name"
                label="Name"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={API.name}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="method"
                label="Method"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={API.method}
              >
                <Select>
                  <Option value="GET">GET</Option>
                  <Option value="POST">POST</Option>
                  <Option value="PUT">PUT</Option>
                  <Option value="DELETE">DELETE</Option>
                </Select>
              </Form.Item>

              <Form.Item
                name="path"
                label="Path"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={API.path}
              >
                <Input />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
                <Space>
                  {handleGetAction('update') && (
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={loadingUpdate}
                    >
                      Update
                    </Button>
                  )}

                  <Button type="default" onClick={handleCancel}>
                    Cancel
                  </Button>
                </Space>
              </Form.Item>
            </Form>
          )}
        </div>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default UpdateAPIsPage;
const Wrapper = styled.div`
  .createAPI {
    background-color: #fff;
    padding: 12px;
    box-shadow: ${({ theme }) => theme.shadowContainer};
    margin-bottom: 12px;

    &-form {
      max-width: 600px;
    }
  }
`;
