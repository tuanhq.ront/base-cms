import { Button, Form, Input, Select, Space } from 'antd';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { APIsService } from 'service/APIsService';
import styled from 'styled-components';
const { Option } = Select;
const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

function CreateAPIsPage() {
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const [loadingCreate, setLoadingCreate] = useState(false);

  const handleCancel = () => {
    navigate(-1);
  };

  const handleFinish = (values: any) => {
    const param = {
      ...values,
    };

    setLoadingCreate(true);
    APIsService.postAPIService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Create API success');
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Create API failed');
      })
      .finally(() => {
        setLoadingCreate(false);
      });
  };

  return (
    <LayoutDashboard>
      <HelmetComponent title="APIs Management | Create API" />
      <TitlePage title="Create API" />

      <Wrapper>
        <div className="createAPI">
          <Form
            {...layout}
            form={form}
            className="createAPI-form"
            onFinish={handleFinish}
          >
            <Form.Item
              name="name"
              label="Name"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="method"
              label="Method"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Select>
                <Option value="GET">GET</Option>
                <Option value="POST">POST</Option>
                <Option value="PUT">PUT</Option>
                <Option value="DELETE">DELETE</Option>
              </Select>
            </Form.Item>

            <Form.Item
              name="path"
              label="Path"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loadingCreate}
                >
                  Create
                </Button>
                <Button type="default" onClick={handleCancel}>
                  Cancel
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </div>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default CreateAPIsPage;
const Wrapper = styled.div`
  .createAPI {
    background-color: #fff;
    padding: 12px;
    box-shadow: ${({ theme }) => theme.shadowContainer};
    margin-bottom: 12px;

    &-form {
      max-width: 600px;
    }
  }
`;
