export interface APITypes {
  method: string;
  path: string;
  name: string;
}
