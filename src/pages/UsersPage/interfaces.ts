export interface User {
  email: string;
  full_name: string;
  roles: any[];
  wallet_address: string;
  organization_id: string;
}
