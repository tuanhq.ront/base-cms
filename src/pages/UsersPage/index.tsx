import { PlusOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Space } from 'antd';
import Table from 'antd/es/table';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';
import styled from 'styled-components';
import { ContentBoxShadow } from 'styledComponents/commonStyled';
import HelmetComponent from '../../components/HelmetComponent';
import LayoutDashboard from '../../components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from '../../components/Notification';
import TitlePage from '../../components/TitlePage';
import { UserService } from '../../service/UserService';

function UserPage() {
  const navigate = useNavigate();

  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);

  const [users, setUsers] = useState([]);
  const [loadingGetUsers, setLoadingGetUsers] = useState<boolean>(false);

  // CHECKING PERMISSION
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, location.pathname);
  };
  //

  const handleOpenCreateUser = () => {
    navigate('/users/create-user');
  };

  const handleUpdateUser = (id: string) => {
    navigate('/users/update-user/' + id);
  };

  const handleDeleteUser = (email: string) => {
    UserService.deleteUserService(email)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Delete user success');
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Delete user failed');
      })
      .finally(() => {
        handleGetAllUsers();
      });
  };

  const handleGetAllUsers = () => {
    setLoadingGetUsers(true);
    UserService.getAllUsersService(page, size)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setUsers(res.data.rows);
        setTotal(res.data.total);
      })
      .catch(err => {
        NotificationError('Error', 'Get list users failed');
        console.error(err);
      })
      .finally(() => {
        setLoadingGetUsers(false);
      });
  };

  useEffect(() => {
    handleGetAllUsers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, size]);

  const columns: any = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'full_name',
      title: 'Full name',
      dataIndex: 'full_name',
    },

    {
      key: 'email',
      title: 'Email',
      dataIndex: 'email',
    },
    {
      key: `action`,
      title: 'Action',
      align: 'center',
      width: 200,

      render: (text: any, record: any) => (
        <Space>
          <Button
            size="small"
            type="primary"
            onClick={() => handleUpdateUser(record.email)}
          >
            Detail
          </Button>

          {handleGetAction('delete') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this user?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleDeleteUser(record.email)}
            >
              <Button size="small" type="primary" danger>
                Delete
              </Button>
            </Popconfirm>
          )}
        </Space>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />

      <Wrapper>
        {handleGetAction('create') && (
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleOpenCreateUser}
          >
            Create
          </Button>
        )}

        <ContentBoxShadow>
          <Table
            rowKey={'email'}
            size="small"
            columns={columns}
            dataSource={users}
            loading={loadingGetUsers}
            pagination={{
              onChange: (page: any, size: any) => {
                setPage(page);
                setSize(size);
              },
              showSizeChanger: true,
              pageSizeOptions: [10, 20, 30],
              current: page,
              pageSize: size,
              total: total,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} items`,
            }}
          />
        </ContentBoxShadow>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default UserPage;

const Wrapper = styled.div`
  .userPage {
    &-table {
      margin-top: 12px;
      background-color: #fff;
      padding: 12px;
      box-shadow: ${({ theme }) => theme.shadowContainer};
      margin-bottom: 12px;
    }

    &-containerTagsRole {
      display: flex;
      justify-content: flex-start;
      align-items: center;
    }

    &-tagsRole {
      border: 1px solid #ccc;
      padding: 0px 5px;
      margin-right: 4px;
      border-radius: 4px;
    }
  }
`;
