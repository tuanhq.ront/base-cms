import { DeleteOutlined } from '@ant-design/icons';
import {
  Button,
  Form,
  Input,
  Popconfirm,
  Select,
  Skeleton,
  Space,
  Table,
} from 'antd';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { OrganizationService } from 'service/OrganizationService';
import styled from 'styled-components';
import { ContentBoxShadow } from 'styledComponents/commonStyled';
import HelmetComponent from '../../../components/HelmetComponent';
import LayoutDashboard from '../../../components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from '../../../components/Notification';
import TitlePage from '../../../components/TitlePage';
import { UserService } from '../../../service/UserService';
import { User } from '../interfaces';
import ModalAddRole from './ModalAddRole';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

function UpdateUserPage() {
  const [form] = Form.useForm();
  const { id } = useParams() as { id: string };
  const navigate = useNavigate();

  const [loadingGetDetail, setLoadingGetDetail] = useState(false);
  const [loadingUpdate, setLoadingUpdate] = useState(false);

  const [newPassword, setNewPassword] = useState('');
  const [loadingChangePassword, setLoadingChangePassword] = useState(false);

  // CHECKING PERMISSION
  const path = '/' + useLocation().pathname.split('/')[1];

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, path);
  };
  //

  const [user, setUser] = useState<User>({
    email: '',
    full_name: '',
    wallet_address: '',
    organization_id: '',
    roles: [],
  });

  const [organizations, setOrganizations] = useState([]);

  // ADD ROLES

  const [visibleAddRole, setVisibleAddRole] = useState(false);
  const [roleSelected, setRoleSelected] = useState<any>([]);

  const handleOpenAddRole = () => {
    setVisibleAddRole(true);
  };

  const handleCloseAddRole = () => {
    setVisibleAddRole(false);
  };

  const addRole = (data: any) => {
    const role = JSON.parse(data);

    const check = roleSelected.some(r => r.id === role.id);

    if (check) {
      return;
    }
    const list = [...roleSelected, role];
    handleUpdateRole(list);
  };

  const deleteRole = (id: string) => {
    const newList = roleSelected.filter(r => r.id !== id);
    handleUpdateRole(newList);
  };

  const handleUpdateRole = (roles: any[]) => {
    const param = {
      id,
      email: user.email,
      full_name: user.full_name,
      wallet_address: user.wallet_address,
      organization: user.organization_id,
      roles: roles.map(r => r.id),
    };

    setLoadingUpdate(true);
    UserService.updateUserService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Update user success');
        handleGetDetailUser();
        // handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Update user failed');
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  const handleGetAllOrganizations = () => {
    OrganizationService.getAllOrganizationsServiceNotPagination()
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setOrganizations(res.data.rows);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {});
  };

  //

  const onFinish = (values: any) => {
    const param = {
      ...values,
      id,
      roles: roleSelected.map(r => r.id),
    };

    setLoadingUpdate(true);
    UserService.updateUserService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Change user success');
        handleGetDetailUser();
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Update user failed');
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  const handleCancel = () => {
    navigate(-1);
  };

  const handleGetDetailUser = () => {
    setLoadingGetDetail(true);
    UserService.getDetailUserService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setUser(res.data.rows[0]);
        setRoleSelected(res.data.rows[0].roles);
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Get detail user failed');
      })
      .finally(() => {
        setLoadingGetDetail(false);
      });
  };

  const handleChangePassword = () => {
    if (newPassword === '' || newPassword.length < 6) {
      NotificationError('Error', 'Password must be at least 6 characters');
      return;
    }

    const param = {
      email: user.email,
      newPassword: newPassword,
    };

    setLoadingChangePassword(true);
    UserService.changePasswordUserService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Change password success');
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Change password failed');
      })
      .finally(() => {
        setLoadingChangePassword(false);
      });
  };

  useEffect(() => {
    handleGetDetailUser();
    handleGetAllOrganizations();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [user]);

  const columns: any = [
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },

    {
      key: `action`,
      title: 'Action',
      width: 50,
      align: 'center',
      render: (text: any, record: any) => (
        <>
          <Popconfirm
            placement="topRight"
            title="Are you sure to delete this role?"
            okText="Yes"
            cancelText="No"
            onConfirm={() => deleteRole(record.id)}
          >
            <Button
              icon={<DeleteOutlined />}
              size="small"
              type="primary"
              danger
            ></Button>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title="Update Users" />
      <TitlePage title="Update Users" />

      <Wrapper>
        <ContentBoxShadow>
          {loadingGetDetail ? (
            <Skeleton />
          ) : (
            <Form
              {...layout}
              form={form}
              onFinish={onFinish}
              className="updateUser-info-form"
            >
              <Form.Item
                name="full_name"
                label="Full name"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={user.full_name}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="email"
                label="Email"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={user.email}
              >
                <Input disabled type="email" />
              </Form.Item>

              {/* <Form.Item
                name="wallet_address"
                label="Wallet Address"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={user.wallet_address}
              >
                <Input />
              </Form.Item> */}

              <Form.Item
                name="organization"
                label="Organization"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={user.organization_id}
              >
                <Select allowClear>
                  {organizations.map((o: any) => (
                    <Select.Option key={o.id} value={o.id}>
                      {o.name}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
                <Space>
                  {handleGetAction('update') && (
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={loadingUpdate}
                    >
                      Update
                    </Button>
                  )}

                  <Button type="default" onClick={handleCancel}>
                    Cancel
                  </Button>
                </Space>
              </Form.Item>
            </Form>
          )}
        </ContentBoxShadow>

        {handleGetAction('change-password') && (
          <div className="updateUser-changePassword">
            <div className="updateUser-label">Change Password</div>
            <Space>
              <Input.Password
                style={{ width: '300px' }}
                value={newPassword}
                onChange={e => {
                  setNewPassword(e.target.value);
                }}
              ></Input.Password>
              <Button
                type="primary"
                onClick={handleChangePassword}
                loading={loadingChangePassword}
              >
                Change Password
              </Button>
            </Space>
          </div>
        )}

        <div className="updateUser-containerRoles">
          <div className="updateUser-label">
            Roles
            <Button
              className="updateUser-button"
              size="middle"
              type="primary"
              onClick={handleOpenAddRole}
            >
              Add
            </Button>
          </div>
          <Table
            rowKey={'id'}
            className="updateUser-table"
            size="small"
            showHeader={false}
            columns={columns}
            dataSource={roleSelected}
            pagination={false}
          />
        </div>
      </Wrapper>

      <ModalAddRole
        visible={visibleAddRole}
        onCancel={handleCloseAddRole}
        callback={addRole}
      />
    </LayoutDashboard>
  );
}

export default UpdateUserPage;

const Wrapper = styled.div`
  .updateUser {
    &-info {
      &-form {
        max-width: 800px;
      }
    }

    &-actions {
      background-color: #fff;
      padding: 12px;
      box-shadow: ${({ theme }) => theme.shadowContainer};

      &-title {
        font-size: 18px;
        margin-bottom: 8px;
        font-weight: 500;
      }

      &-button {
        margin-bottom: 10px;
      }
    }

    &-changePassword,
    &-containerRoles {
      box-shadow: ${({ theme }) => theme.shadowContainer};
      background-color: #fff;
      padding: 12px;
      margin-bottom: 12px;
    }

    &-label {
      font-size: 16px;
      font-weight: 500;
    }

    &-table {
      max-width: 800px;
      margin: 10px 0px;
    }

    &-button {
      margin-left: 10px;
    }
  }
`;
