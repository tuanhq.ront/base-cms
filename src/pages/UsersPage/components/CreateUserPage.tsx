import { Button, Form, Input, Select, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { OrganizationService } from 'service/OrganizationService';
import { RolesService } from 'service/RolesService';
import styled from 'styled-components';
import { ContentBoxShadow } from 'styledComponents/commonStyled';
import HelmetComponent from '../../../components/HelmetComponent';
import LayoutDashboard from '../../../components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from '../../../components/Notification';
import TitlePage from '../../../components/TitlePage';
import { UserService } from '../../../service/UserService';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

function CreateUserPage() {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  // const dataAccount = LoginService.getDataLocalStorage();

  const [loadingCreate, setLoadingCreate] = useState(false);

  const [rolesData, setRolesData] = useState([]);
  const [organizations, setOrganizations] = useState([]);

  const handleFinish = (values: any) => {
    const param = {
      ...values,
    };

    setLoadingCreate(true);
    UserService.postUserService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Create user success');
        navigate('/users');
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        setLoadingCreate(false);
      });
  };

  const handleCancel = () => {
    navigate(-1);
  };

  const getAllRoles = () => {
    RolesService.getAllRoleServiceNotPagination()
      .then(res => {
        if (res.data.code !== 0) {
          return false;
        }

        setRolesData(res.data.rows);
      })
      .catch(err => {
        console.error(err);
      });
  };

  const handleGetAllOrganizations = () => {
    OrganizationService.getAllOrganizationsServiceNotPagination()
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setOrganizations(res.data.rows);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {});
  };

  useEffect(() => {
    getAllRoles();
    handleGetAllOrganizations();
  }, []);

  return (
    <LayoutDashboard>
      <HelmetComponent title="Roles Management | Create User" />
      <TitlePage title="Create User" />

      <Wrapper>
        <ContentBoxShadow>
          <Form
            {...layout}
            form={form}
            className="createUser-form"
            onFinish={handleFinish}
          >
            <Form.Item
              name="full_name"
              label="Full name"
              labelAlign="left"
              rules={[
                { required: true, message: 'This field is require!' },
                {
                  pattern:
                    /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s ]+$/,
                  message: 'Invalid full name',
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="email"
              label="Email"
              labelAlign="left"
              rules={[
                { required: true, message: 'This field is require!' },
                {
                  pattern: /^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/,
                  message: 'Invalid email',
                },
              ]}
            >
              <Input type="email" />
            </Form.Item>

            <Form.Item
              name="password"
              label="Password"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input.Password autoComplete="new-password" />
            </Form.Item>

            {/* <Form.Item
              name="wallet_address"
              label="Wallet Address"
              labelAlign="left"
              // rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input />
            </Form.Item> */}

            <Form.Item
              name="roles"
              label="Role"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Select mode="multiple" allowClear>
                {rolesData.map((role: any) => (
                  <Select.Option key={role.id} value={role.id}>
                    {role.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item
              name="organization"
              label="Organization"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Select allowClear>
                {organizations.map((o: any) => (
                  <Select.Option key={o.id} value={o.id}>
                    {o.name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loadingCreate}
                >
                  Create
                </Button>
                <Button type="default" onClick={handleCancel}>
                  Cancel
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </ContentBoxShadow>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default CreateUserPage;

const Wrapper = styled.div`
  .createUser {
    &-form {
      max-width: 800px;
    }

    &-label {
      font-size: 16px;
      font-weight: 500;
    }

    &-label {
      font-size: 16px;
      font-weight: 500;
    }

    &-table {
      max-width: 800px;
      margin: 10px 0px;
    }

    &-button {
      margin-left: 10px;
    }
  }
`;
