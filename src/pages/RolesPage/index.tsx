import { PlusOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Space, Table } from 'antd';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';
import { RolesService } from 'service/RolesService';
import styled from 'styled-components';

function RolesPage() {
  const navigate = useNavigate();

  const [roles, setRoles] = useState([]);

  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);

  const [loadingGetAll, setLoadingGetAll] = useState(false);

  // CHECKING PERMISSION
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  const handleCheckAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, location.pathname);
  };
  //

  const handleCreateRoles = () => {
    navigate('/roles/create');
  };

  const handleUpdateRole = (id: string) => {
    navigate('/roles/update/' + id);
  };

  const handleDeleteRole = (id: string) => {
    RolesService.deleteRoleService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Delete Role success');
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Delete Role failed');
      })
      .finally(() => {
        handleGetAllRoles();
      });
  };

  const handleGetAllRoles = () => {
    setLoadingGetAll(true);
    RolesService.getAllRoleService(page, size)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setRoles(res.data.rows);
        setTotal(res.data.total);
      })
      .catch(err => {
        NotificationError('Error', 'Get list roles failed');
      })
      .finally(() => {
        setLoadingGetAll(false);
      });
  };

  useEffect(() => {
    handleGetAllRoles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, size]);

  const columns: any = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },

    {
      key: `action`,
      title: 'Action',
      width: 200,
      align: 'center',
      render: (text: any, record: any) => (
        <Space>
          <Button
            size="small"
            type="primary"
            onClick={() => handleUpdateRole(record.id)}
          >
            Detail
          </Button>

          {handleCheckAction('delete') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this role?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleDeleteRole(record.id)}
            >
              <Button size="small" type="primary" danger>
                Delete
              </Button>
            </Popconfirm>
          )}
        </Space>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />

      {handleCheckAction('create') && (
        <Button
          type="primary"
          icon={<PlusOutlined />}
          onClick={handleCreateRoles}
        >
          Create
        </Button>
      )}

      <Wrapper>
        <div className="RolesPage-table">
          <Table
            rowKey={'id'}
            size="small"
            columns={columns}
            dataSource={roles}
            loading={loadingGetAll}
            pagination={{
              onChange: (page: any, size: any) => {
                setPage(page);
                setSize(size);
              },
              showSizeChanger: true,
              pageSizeOptions: [10, 20, 30],
              current: page,
              pageSize: size,
              total: total,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} roles`,
            }}
          />
        </div>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default RolesPage;

const Wrapper = styled.div`
  .RolesPage {
    &-table {
      background-color: #fff;
      padding: 12px;
      box-shadow: ${({ theme }) => theme.shadowContainer};
      margin-bottom: 12px;
      margin-top: 12px;
    }
  }
`;
