import { Button, Modal, Select } from 'antd';
import { NotificationError } from 'components/Notification';
import React, { useEffect, useState } from 'react';
import { ViewRolesService } from 'service/ViewRolesService';
import styled from 'styled-components';
const { Option } = Select;

interface Props {
  visible: boolean;
  onCancel: () => void;
  callback: (data: string) => void;
}

function ModalAddViewRoles(props: Props) {
  const { visible, onCancel, callback } = props;

  const [roles, setRoles] = useState([]);
  const [value, setValue] = useState('');

  const handleSelect = value => {
    callback(value);
    setValue(value);
    onCancel();
  };

  useEffect(() => {
    if (!visible) {
      return;
    }

    ViewRolesService.getAllViewRolesServiceNotPagination()
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return false;
        }

        setRoles(res.data.rows);
        setValue('');
      })
      .catch(err => {
        console.error(err);
      });
  }, [visible]);

  return (
    <Wrapper>
      <Modal
        title="View Roles"
        visible={visible}
        onCancel={onCancel}
        footer={<Button onClick={onCancel}>Cancel</Button>}
      >
        <Select
          showSearch
          style={{ width: '100%' }}
          onChange={handleSelect}
          value={value}
        >
          {roles.map((role: any, index: any) => (
            <Option key={index} value={JSON.stringify(role)}>
              {role.name}
            </Option>
          ))}
        </Select>
      </Modal>
    </Wrapper>
  );
}

export default ModalAddViewRoles;

const Wrapper = styled.div``;
