import { DeleteOutlined } from '@ant-design/icons';
import { Button, Form, Input, Popconfirm, Skeleton, Space, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { RolesService } from 'service/RolesService';
import styled from 'styled-components';
import { RoleTypes } from '../interfaces';
import ModalAddAPIRoles from './ModalAddAPIRoles';
import ModalAddViewRoles from './ModalAddViewRoles';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

interface UpdateAPIs {
  api_roles: any[];
  view_roles: any[];
}

function UpdateRolesPage() {
  const { id } = useParams() as { id: string };
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const [role, setRole] = useState<RoleTypes>({
    name: '',
    api_roles: [],
    description: '',
    view_roles: [],
  });

  const [loadingGetDetail, setLoadingGetDetail] = useState(false);
  const [loadingUpdate, setLoadingUpdate] = useState(false);

  const [apiRolesSelected, setApiRolesSelected] = useState<any>([]);
  const [viewRolesSelected, setViewRolesSelected] = useState<any>([]);

  // CHECKING PERMISSION
  const path = '/' + useLocation().pathname.split('/')[1];

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, path);
  };
  //

  // ADD API ROLES
  const [visibleAddAPIRoles, setVisibleAddAPIRoles] = useState(false);

  const handleOpenAddAPIRoles = () => {
    setVisibleAddAPIRoles(true);
  };

  const handleCloseAddAPIRoles = () => {
    setVisibleAddAPIRoles(false);
  };

  const handleAddAPIRoles = (data: string) => {
    const role = JSON.parse(data);

    const check = apiRolesSelected.some(api => api.id === role.id);

    if (check) {
      return;
    }

    const params: UpdateAPIs = {
      api_roles: [...apiRolesSelected, role],
      view_roles: viewRolesSelected,
    };

    handleUpdateAPIs(params);
  };

  const handleDeleteAPIRole = (id: string) => {
    const newList = apiRolesSelected.filter(api => api.id !== id);

    const params: UpdateAPIs = {
      api_roles: newList,
      view_roles: viewRolesSelected,
    };

    handleUpdateAPIs(params);
  };
  //

  // ADD VIEW ROLES
  const [visibleAddViewRole, setVisibleAddViewRole] = useState(false);

  const handleOpenAddViewRoles = () => {
    setVisibleAddViewRole(true);
  };

  const handleCloseAddViewRoles = () => {
    setVisibleAddViewRole(false);
  };

  const handleAddViewRoles = (data: string) => {
    const view = JSON.parse(data);

    const check = viewRolesSelected.some(api => api.id === view.id);

    if (check) {
      return;
    }

    const params: UpdateAPIs = {
      api_roles: apiRolesSelected,
      view_roles: [...viewRolesSelected, view],
    };

    handleUpdateAPIs(params);
  };

  const handleDeleteViewRole = (id: string) => {
    const newList = viewRolesSelected.filter(api => api.id !== id);

    const params: UpdateAPIs = {
      api_roles: apiRolesSelected,
      view_roles: newList,
    };

    handleUpdateAPIs(params);
  };
  //

  const handleCancel = () => {
    navigate(-1);
  };

  const handleUpdateAPIs = (values: UpdateAPIs) => {
    const params = {
      id,
      name: role.name,
      description: role.description,
      view_roles: values.view_roles.map(api => api.id),
      api_roles: values.api_roles.map(api => api.id),
    };

    setLoadingUpdate(true);
    RolesService.updateRoleService(params)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Update role success');
        handleGetDetailRole();
        // handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Update role failed');
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  const handleFinish = (values: any) => {
    const param = {
      ...values,
      id,
      api_roles: apiRolesSelected.map(api => api.id),
      view_roles: viewRolesSelected.map(api => api.id),
    };

    setLoadingUpdate(true);
    RolesService.updateRoleService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Update role success');
        handleGetDetailRole();
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Update role failed');
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  const handleGetDetailRole = () => {
    setLoadingGetDetail(true);
    RolesService.getDetailRoleService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setRole(res.data.rows[0]);
        setApiRolesSelected(res.data.rows[0].api_roles);
        setViewRolesSelected(res.data.rows[0].view_roles);
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Get detail role failed');
      })
      .finally(() => {
        setLoadingGetDetail(false);
      });
  };

  useEffect(() => {
    handleGetDetailRole();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [role]);

  const columnsAPI: ColumnsType<any> = [
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },
    {
      key: 'action',
      title: 'Action',
      width: 50,
      align: 'center',
      render: (text: any, record: any) => (
        <>
          {handleGetAction('delete-api-role') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this role?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleDeleteAPIRole(record.id)}
            >
              <Button
                danger
                type="primary"
                icon={<DeleteOutlined />}
                size="small"
              />
            </Popconfirm>
          )}
        </>
      ),
    },
  ];

  const columnsView: ColumnsType<any> = [
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },
    {
      key: 'action',
      title: 'Action',
      width: 50,
      align: 'center',
      render: (text: any, record: any) => (
        <>
          {handleGetAction('delete-view-role') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this role?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleDeleteViewRole(record.id)}
            >
              <Button
                danger
                type="primary"
                icon={<DeleteOutlined />}
                size="small"
              />
            </Popconfirm>
          )}
        </>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title="Update Roles" />
      <TitlePage title="Update Roles" />

      <Wrapper>
        <div className="updateRoles">
          {loadingGetDetail ? (
            <Skeleton />
          ) : (
            <Form
              {...layout}
              form={form}
              className="updateRoles-form"
              onFinish={handleFinish}
            >
              <Form.Item
                name="name"
                label="Name"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={role.name}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="description"
                label="Description"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={role.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
                <Space>
                  {handleGetAction('update') && (
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={loadingUpdate}
                    >
                      Update
                    </Button>
                  )}

                  <Button type="default" onClick={handleCancel}>
                    Cancel
                  </Button>
                </Space>
              </Form.Item>
            </Form>
          )}
        </div>

        <div className="updateRoles-containerRoles">
          <div className="updateRoles-role">
            <div className="updateRoles-label">
              API Roles
              {handleGetAction('add-api-role') && (
                <Button
                  className="updateRoles-button"
                  size="middle"
                  type="primary"
                  onClick={handleOpenAddAPIRoles}
                >
                  Add
                </Button>
              )}
            </div>

            <Table
              rowKey={'id'}
              className="updateRoles-table"
              size="small"
              showHeader={false}
              columns={columnsAPI}
              dataSource={apiRolesSelected}
              pagination={false}
            />
          </div>

          <div className="updateRoles-role">
            <div className="updateRoles-label">
              View Roles
              {handleGetAction('add-view-role') && (
                <Button
                  className="updateRoles-button"
                  size="middle"
                  type="primary"
                  onClick={handleOpenAddViewRoles}
                >
                  Add
                </Button>
              )}
            </div>

            <Table
              rowKey={'id'}
              className="updateRoles-table"
              size="small"
              showHeader={false}
              columns={columnsView}
              dataSource={viewRolesSelected}
              pagination={false}
            />
          </div>
        </div>
      </Wrapper>

      <ModalAddAPIRoles
        visible={visibleAddAPIRoles}
        onCancel={handleCloseAddAPIRoles}
        callback={handleAddAPIRoles}
      />

      <ModalAddViewRoles
        visible={visibleAddViewRole}
        onCancel={handleCloseAddViewRoles}
        callback={handleAddViewRoles}
      />
    </LayoutDashboard>
  );
}

export default UpdateRolesPage;

const Wrapper = styled.div`
  .updateRoles {
    background-color: #fff;
    padding: 12px;
    box-shadow: ${({ theme }) => theme.shadowContainer};
    margin-bottom: 12px;

    &-form {
      max-width: 600px;
    }

    &-containerRoles {
      display: flex;
      justify-content: space-between;
    }

    &-role {
      box-shadow: ${({ theme }) => theme.shadowContainer};
      background-color: #fff;
      padding: 12px;
      width: 49%;
    }

    &-label {
      font-size: 16px;
      font-weight: 500;
    }

    &-table {
      margin: 10px 0px;
    }

    &-button {
      margin-left: 10px;
    }
  }
`;
