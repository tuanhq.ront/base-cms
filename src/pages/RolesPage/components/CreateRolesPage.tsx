import { DeleteOutlined } from '@ant-design/icons';
import { Button, Form, Input, Popconfirm, Space, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { RolesService } from 'service/RolesService';
import styled from 'styled-components';
import ModalAddAPIRoles from './ModalAddAPIRoles';
import ModalAddViewRoles from './ModalAddViewRoles';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

function CreateRolesPage() {
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const [loadingCreate, setLoadingCreate] = useState(false);

  const [apiRolesSelected, setApiRolesSelected] = useState<any>([]);
  const [viewRolesSelected, setViewRolesSelected] = useState<any>([]);

  // ADD API ROLES
  const [visibleAddAPIRoles, setVisibleAddAPIRoles] = useState(false);

  const handleOpenAddAPIRoles = () => {
    setVisibleAddAPIRoles(true);
  };

  const handleCloseAddAPIRoles = () => {
    setVisibleAddAPIRoles(false);
  };

  const handleAddAPIRoles = (data: string) => {
    const role = JSON.parse(data);

    const check = apiRolesSelected.some(api => api.id === role.id);

    if (check) {
      return;
    }

    setApiRolesSelected([...apiRolesSelected, role]);
  };

  const handleDeleteAPIRole = (id: string) => {
    const newList = apiRolesSelected.filter(api => api.id !== id);
    setApiRolesSelected(newList);
  };
  //

  // ADD VIEW ROLES
  const [visibleAddViewRole, setVisibleAddViewRole] = useState(false);

  const handleOpenAddViewRoles = () => {
    setVisibleAddViewRole(true);
  };

  const handleCloseAddViewRoles = () => {
    setVisibleAddViewRole(false);
  };

  const handleAddViewRoles = (data: string) => {
    const view = JSON.parse(data);

    const check = viewRolesSelected.some(api => api.id === view.id);

    if (check) {
      return;
    }

    setViewRolesSelected([...viewRolesSelected, view]);
  };

  const handleDeleteViewRole = (id: string) => {
    const newList = viewRolesSelected.filter(api => api.id !== id);
    setViewRolesSelected(newList);
  };
  //

  const handleCancel = () => {
    navigate(-1);
  };

  const handleFinish = (values: any) => {
    const param = {
      ...values,
      api_roles: apiRolesSelected.map(api => api.id),
      view_roles: viewRolesSelected.map(api => api.id),
    };

    setLoadingCreate(true);
    RolesService.postRoleService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Create role success');
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Create role failed');
      })
      .finally(() => {
        setLoadingCreate(false);
      });
  };

  const columnsAPI: ColumnsType<any> = [
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },
    {
      key: 'action',
      title: 'Action',
      width: 50,
      align: 'center',
      render: (text: any, record: any) => (
        <Popconfirm
          placement="topRight"
          title="Are you sure to delete this role?"
          okText="Yes"
          cancelText="No"
          onConfirm={() => handleDeleteAPIRole(record.id)}
        >
          <Button
            danger
            type="primary"
            icon={<DeleteOutlined />}
            size="small"
          />
        </Popconfirm>
      ),
    },
  ];

  const columnsView: ColumnsType<any> = [
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },
    {
      key: 'action',
      title: 'Action',
      width: 50,
      align: 'center',
      render: (text: any, record: any) => (
        <>
          <Popconfirm
            placement="topRight"
            title="Are you sure to delete this role?"
            okText="Yes"
            cancelText="No"
            onConfirm={() => handleDeleteViewRole(record.id)}
          >
            <Button
              danger
              type="primary"
              icon={<DeleteOutlined />}
              size="small"
            />
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title="Create Roles" />
      <TitlePage title="Create Roles" />

      <Wrapper>
        <div className="createRoles">
          <Form
            {...layout}
            form={form}
            className="createRoles-form"
            onFinish={handleFinish}
          >
            <Form.Item
              name="name"
              label="Name"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="description"
              label="Description"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input.TextArea rows={4} />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loadingCreate}
                >
                  Create
                </Button>
                <Button type="default" onClick={handleCancel}>
                  Cancel
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </div>

        <div className="createRoles-containerRoles">
          <div className="createRoles-role">
            <div className="createRoles-label">
              API Roles
              <Button
                className="createRoles-button"
                size="middle"
                type="primary"
                onClick={handleOpenAddAPIRoles}
              >
                Add
              </Button>
            </div>

            <Table
              rowKey={'id'}
              className="createRoles-table"
              size="small"
              showHeader={false}
              columns={columnsAPI}
              dataSource={apiRolesSelected}
              pagination={false}
            />
          </div>

          <div className="createRoles-role">
            <div className="createRoles-label">
              View Roles{' '}
              <Button
                className="createRoles-button"
                size="middle"
                type="primary"
                onClick={handleOpenAddViewRoles}
              >
                Add
              </Button>
            </div>

            <Table
              rowKey={'id'}
              className="createRoles-table"
              size="small"
              showHeader={false}
              columns={columnsView}
              dataSource={viewRolesSelected}
              pagination={false}
            />
          </div>
        </div>
      </Wrapper>

      <ModalAddAPIRoles
        visible={visibleAddAPIRoles}
        onCancel={handleCloseAddAPIRoles}
        callback={handleAddAPIRoles}
      />

      <ModalAddViewRoles
        visible={visibleAddViewRole}
        onCancel={handleCloseAddViewRoles}
        callback={handleAddViewRoles}
      />
    </LayoutDashboard>
  );
}

const Wrapper = styled.div`
  .createRoles {
    background-color: #fff;
    padding: 12px;
    box-shadow: ${({ theme }) => theme.shadowContainer};
    margin-bottom: 12px;

    &-form {
      max-width: 800px;
    }

    &-containerRoles {
      display: flex;
      justify-content: space-between;
    }

    &-role {
      box-shadow: ${({ theme }) => theme.shadowContainer};
      background-color: #fff;
      padding: 12px;
      width: 49%;
    }

    &-label {
      font-size: 16px;
      font-weight: 500;
    }

    &-table {
      margin: 10px 0px;
    }

    &-button {
      margin-left: 10px;
    }
  }
`;

export default CreateRolesPage;
