export interface View {
  title: string;
  path: string;
  icon: string;
  key: string;
  index: number;
  actions: any[];
}
