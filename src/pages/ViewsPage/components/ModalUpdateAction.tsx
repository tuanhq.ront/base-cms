import { Button, Form, Input, Modal, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import {
  NotificationError,
  NotificationSuccess,
} from '../../../components/Notification';
import { ViewsService } from '../../../service/ViewsService';

interface Props {
  visible: boolean;
  handleCancel: () => void;
  data: any;
  idPermission: any;
  handleRefresh: () => void;
}

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

function ModalUpdateAction(props: Props) {
  const { visible, handleCancel, data, idPermission, handleRefresh } = props;
  const [form] = Form.useForm();

  const [loadingUpdate, setLoadingUpdate] = useState(false);

  const onFinish = (values: any) => {
    const param = { id: data.id, ...values, permission_id: idPermission };

    setLoadingUpdate(true);
    ViewsService.updateActionForPermission(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Update action success');
        handleCancel();
        handleRefresh();
      })
      .catch(err => {
        NotificationError('Error', 'Update action failed');
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [data]);

  return (
    <Modal
      title="Create Action"
      visible={visible}
      onCancel={() => {
        handleCancel();
      }}
      afterClose={() => {
        form.resetFields();
      }}
      footer={false}
      maskClosable={false}
    >
      <Form
        {...layout}
        form={form}
        onFinish={onFinish}
        className="updateAction-info-form"
      >
        <Form.Item
          name="title"
          label="Title"
          labelAlign="left"
          rules={[{ required: true, message: 'This field is require!' }]}
          initialValue={data.title}
        >
          <Input />
        </Form.Item>

        {/* <Form.Item
          name="path"
          label="Path"
          labelAlign="left"
          rules={[{ required: true, message: 'This field is require!' }]}
          initialValue={data.path}
        >
          <Input />
        </Form.Item> */}

        <Form.Item
          name="key"
          label="Key"
          labelAlign="left"
          rules={[{ required: true, message: 'This field is require!' }]}
          initialValue={data.key}
        >
          <Input />
        </Form.Item>

        {/* <Form.Item
          name="icon"
          label="Icon"
          labelAlign="left"
          rules={[{ required: true, message: 'This field is require!' }]}
          initialValue={data.icon}
        >
          <Input />
        </Form.Item> */}

        <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
          <Space>
            <Button type="primary" htmlType="submit" loading={loadingUpdate}>
              Save
            </Button>
            <Button type="default" onClick={handleCancel}>
              Cancel
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default ModalUpdateAction;
