import {
  Button,
  Form,
  Input,
  InputNumber,
  Popconfirm,
  Skeleton,
  Space,
  Table,
} from 'antd';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import styled from 'styled-components';
import { ContentBoxShadow } from 'styledComponents/commonStyled';
import HelmetComponent from '../../../components/HelmetComponent';
import LayoutDashboard from '../../../components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from '../../../components/Notification';
import TitlePage from '../../../components/TitlePage';
import { ViewsService } from '../../../service/ViewsService';
import { View } from '../interfaces';
import ModalCreateAction from './ModalCreateAction';
import ModalUpdateAction from './ModalUpdateAction';

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

function UpdateViewPage() {
  const [form] = Form.useForm();
  const { id } = useParams() as { id: string };
  const navigate = useNavigate();

  const [loadingUpdateView, setLoadingUpdateView] = useState(false);
  const [loadingGetDetail, setLoadingGetDetail] = useState(false);

  const [view, setView] = useState<View>({
    title: '',
    path: '',
    icon: '',
    key: '',
    index: 0,
    actions: [],
  });

  const [visibleCreateAction, setVisibleCreateAction] = useState(false);

  const [visibleUpdateAction, setVisibleUpdateAction] = useState(false);
  const [actionUpdate, setActionUpdate] = useState<any>({});

  // CHECKING PERMISSION
  const path = '/' + useLocation().pathname.split('/')[1];

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, path);
  };
  //

  // CREATE ACTIONS
  const handleOpenCreateAction = () => {
    setVisibleCreateAction(true);
  };

  const handleCloseCreateAction = () => {
    setVisibleCreateAction(false);
  };

  // UPDATE ACTIONS
  const handleOpenUpdateAction = (action: any) => {
    setActionUpdate(action);
    setVisibleUpdateAction(true);
  };

  const handleCloseUpdateAction = () => {
    setVisibleUpdateAction(false);
    setActionUpdate({});
  };

  // DELETE ACTIONS

  const handleDeleteAction = (action_id: string) => {
    ViewsService.deletesActionService(action_id, id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Delete action success');
        handleGetDetailView();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Delete action failed');
      });
  };
  // PERMISSIONS

  const onFinish = (values: any) => {
    const data = { ...values, id: id, actions: view.actions };

    setLoadingUpdateView(true);
    ViewsService.updateViewService(data)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Update view success');
        handleGetDetailView();
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Update view failed');
      })
      .finally(() => {
        setLoadingUpdateView(false);
      });
  };

  const handleCancel = () => {
    navigate(-1);
  };

  const handleGetDetailView = () => {
    setLoadingGetDetail(true);
    ViewsService.getDetailView(id)
      .then(res => {
        if (res.data.code !== 0) {
          return;
        }
        const data = res.data.rows[0];
        setView(data);
      })
      .catch(err => {
        console.log(err);
        NotificationError('Error', '');
      })
      .finally(() => {
        setLoadingGetDetail(false);
      });
  };

  useEffect(() => {
    handleGetDetailView();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [view]);

  const columns: any = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'title',
      title: 'Title',
      dataIndex: 'title',
    },
    {
      key: 'key',
      title: 'Key',
      dataIndex: 'key',
    },
    {
      key: `${
        handleGetAction('update-action') || handleGetAction('delete-action')
          ? 'action'
          : 'hidden'
      }`,
      width: 200,
      title: 'Action',
      align: 'center',
      render: (text: any, record: any) => (
        <Space>
          {handleGetAction('update-action') && (
            <Button
              size="small"
              type="primary"
              onClick={() => handleOpenUpdateAction(record)}
            >
              Update
            </Button>
          )}

          {handleGetAction('delete-action') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this action?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleDeleteAction(record.id)}
            >
              <Button size="small" type="primary" danger>
                Delete
              </Button>
            </Popconfirm>
          )}
        </Space>
      ),
    },
  ].filter(col => col.key !== 'hidden');

  return (
    <LayoutDashboard>
      <HelmetComponent title="Update View" />
      <TitlePage title="Update View" />

      <Wrapper>
        <ContentBoxShadow>
          {loadingGetDetail ? (
            <Skeleton />
          ) : (
            <Form
              {...layout}
              form={form}
              name="updateView"
              onFinish={onFinish}
              className="updateView-info-form"
            >
              <Form.Item
                name="title"
                label="Title"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={view.title}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="path"
                label="Path"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={view.path}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="index"
                label="Index"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={view.index}
              >
                <InputNumber style={{ width: '100%' }} />
              </Form.Item>

              <Form.Item
                name="icon"
                label="Icon"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={view.icon}
              >
                <Input />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                <Space>
                  {handleGetAction('update') && (
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={loadingUpdateView}
                    >
                      Update
                    </Button>
                  )}

                  <Button type="default" onClick={handleCancel}>
                    Cancel
                  </Button>
                </Space>
              </Form.Item>
            </Form>
          )}
        </ContentBoxShadow>

        <div className="updateView-actions">
          {loadingGetDetail ? (
            <Skeleton />
          ) : (
            <>
              <div className="updateView-actions-title">Actions</div>

              {handleGetAction('create-action') && (
                <Button
                  type="primary"
                  className="updateView-actions-button"
                  onClick={handleOpenCreateAction}
                >
                  Create Action
                </Button>
              )}

              <Table
                rowKey="id"
                size="small"
                columns={columns}
                dataSource={view.actions}
                pagination={{
                  onChange: page => {},
                  pageSize: 10,
                  total: view.actions.length,
                  showTotal: (total, range) =>
                    `${range[0]}-${range[1]} of ${total} actions`,
                }}
              />
            </>
          )}
        </div>
      </Wrapper>

      <ModalCreateAction
        visible={visibleCreateAction}
        handleCancel={handleCloseCreateAction}
        idPermission={id}
        handleRefresh={handleGetDetailView}
      />

      <ModalUpdateAction
        visible={visibleUpdateAction}
        handleCancel={handleCloseUpdateAction}
        idPermission={id}
        data={actionUpdate}
        handleRefresh={handleGetDetailView}
      />
    </LayoutDashboard>
  );
}

export default UpdateViewPage;

const Wrapper = styled.div`
  .updateView {
    &-info {
      &-form {
        max-width: 600px;
      }
    }

    &-actions {
      background-color: #fff;
      padding: 12px;
      box-shadow: ${({ theme }) => theme.shadowContainer};

      &-title {
        font-size: 18px;
        margin-bottom: 8px;
        font-weight: 500;
      }

      &-button {
        margin-bottom: 10px;
      }
    }
  }
`;
