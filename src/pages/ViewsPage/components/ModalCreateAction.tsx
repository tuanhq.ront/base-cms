import { Button, Form, Input, Modal, Space } from 'antd';
import React, { useState } from 'react';
import {
  NotificationError,
  NotificationSuccess,
} from '../../../components/Notification';
import { ViewsService } from '../../../service/ViewsService';

interface Props {
  visible: boolean;
  handleCancel: () => void;
  idPermission: any;
  handleRefresh: () => void;
}

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

function ModalCreateAction(props: Props) {
  const { visible, handleCancel, idPermission, handleRefresh } = props;
  const [form] = Form.useForm();

  const [loadingCreate, setLoadingCreate] = useState(false);

  const onFinish = (values: any) => {
    const param = { permission_id: idPermission, ...values };

    setLoadingCreate(true);
    ViewsService.createActionForPermission(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Create action success');
        handleRefresh();
        handleCancel();
      })
      .catch(err => {
        NotificationError('Error', 'Create action failed');
      })
      .finally(() => {
        setLoadingCreate(false);
      });
  };

  return (
    <Modal
      title="Create Action"
      visible={visible}
      onCancel={() => {
        handleCancel();
      }}
      afterClose={() => {
        form.resetFields();
      }}
      footer={false}
      maskClosable={false}
    >
      <Form
        {...layout}
        form={form}
        onFinish={onFinish}
        className="createAction-info-form"
      >
        <Form.Item
          name="title"
          label="Title"
          labelAlign="left"
          rules={[{ required: true, message: 'This field is require!' }]}
        >
          <Input />
        </Form.Item>

        {/* <Form.Item
          name="path"
          label="Path"
          labelAlign="left"
          rules={[{ required: true, message: 'This field is require!' }]}
        >
          <Input />
        </Form.Item> */}

        <Form.Item
          name="key"
          label="Key"
          labelAlign="left"
          rules={[{ required: true, message: 'This field is require!' }]}
        >
          <Input />
        </Form.Item>

        {/* <Form.Item
          name="icon"
          label="Icon"
          labelAlign="left"
          rules={[{ required: true, message: 'This field is require!' }]}
        >
          <Input />
        </Form.Item> */}

        <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
          <Space>
            <Button type="primary" htmlType="submit" loading={loadingCreate}>
              Create
            </Button>
            <Button type="default" onClick={handleCancel}>
              Cancel
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default ModalCreateAction;
