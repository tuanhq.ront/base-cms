import { Button, Form, Input, InputNumber, Space } from 'antd';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import { ContentBoxShadow } from 'styledComponents/commonStyled';
import HelmetComponent from '../../../components/HelmetComponent';
import LayoutDashboard from '../../../components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from '../../../components/Notification';
import TitlePage from '../../../components/TitlePage';
import { ViewsService } from '../../../service/ViewsService';

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

function CreateViewPage() {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [loadingCreate, setLoadingCreate] = useState(false);

  const onFinish = (values: any) => {
    setLoadingCreate(true);
    const param = { ...values };

    ViewsService.postViewService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Create view success');
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Create view failed');
      })
      .finally(() => {
        setLoadingCreate(false);
      });
  };

  const handleCancel = () => {
    navigate(-1);
  };

  return (
    <LayoutDashboard>
      <HelmetComponent title="Create View" />
      <TitlePage title="Create View" />

      <Wrapper>
        <ContentBoxShadow>
          <Form
            {...layout}
            form={form}
            onFinish={onFinish}
            className="createView-info-form"
          >
            <Form.Item
              name="title"
              label="Title"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="path"
              label="Path"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="index"
              label="Index"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <InputNumber style={{ width: '100%' }} />
            </Form.Item>

            <Form.Item
              name="icon"
              label="Icon"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loadingCreate}
                >
                  Create
                </Button>
                <Button type="default" onClick={handleCancel}>
                  Cancel
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </ContentBoxShadow>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default CreateViewPage;

const Wrapper = styled.div`
  .createView {
    &-info {
      &-form {
        max-width: 600px;
      }
    }
  }
`;
