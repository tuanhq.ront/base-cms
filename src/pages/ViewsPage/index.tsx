import { PlusOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Space, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';
import { ContentBoxShadow } from 'styledComponents/commonStyled';
import HelmetComponent from '../../components/HelmetComponent';
import LayoutDashboard from '../../components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from '../../components/Notification';
import TitlePage from '../../components/TitlePage';
import { ViewsService } from '../../service/ViewsService';

function ViewPage() {
  const navigate = useNavigate();

  const [views, setViews] = useState<any[]>([]);

  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);

  const [loadingGetAll, setLoadingGetAll] = useState(false);

  // CHECKING PERMISSION
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  const handleCheckAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, location.pathname);
  };
  //

  const handleOpenUpdateView = (view: any) => {
    navigate('/views/update/' + view.id);
  };

  const handleCreateView = () => {
    navigate('/views/create');
  };

  const handleDeleteView = (id: string) => {
    ViewsService.deleteViewService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Delete view success');
        handleGetAllViews();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Delete view failed');
      })
      .finally(() => {});
  };

  const handleGetAllViews = () => {
    setLoadingGetAll(true);
    ViewsService.getAllViewsService(page, size)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setViews(res.data.rows);
        setTotal(res.data.total);
      })
      .catch(err => {
        NotificationError('Error', 'Get list view failed');
      })
      .finally(() => {
        setLoadingGetAll(false);
      });
  };

  useEffect(() => {
    handleGetAllViews();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, size]);

  const columns: ColumnsType<any> = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      dataIndex: 'index',
      render: (text: any) => <>{text}</>,
    },
    {
      key: 'title',
      title: 'Title',
      dataIndex: 'title',
    },
    {
      key: 'icon',
      title: 'Icon',
      dataIndex: 'icon',
      render: (text: any) => <div dangerouslySetInnerHTML={{ __html: text }} />,
    },
    {
      key: 'path',
      title: 'Path',
      dataIndex: 'path',
    },
    {
      key: 'action',
      title: 'Action',
      width: 200,
      align: 'center',
      render: (text: any, record: any) => (
        <Space>
          <Button
            size="small"
            type="primary"
            onClick={() => handleOpenUpdateView(record)}
          >
            Detail
          </Button>

          {handleCheckAction('delete') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this view?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleDeleteView(record.id)}
            >
              <Button type="primary" size="small" danger>
                {handleCheckAction('delete')}
              </Button>
            </Popconfirm>
          )}
        </Space>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />

      {handleCheckAction('create') && (
        <Button
          type="primary"
          icon={<PlusOutlined />}
          onClick={handleCreateView}
        >
          Create
        </Button>
      )}

      <ContentBoxShadow>
        <Table
          rowKey="id"
          size="small"
          columns={columns}
          dataSource={views}
          loading={loadingGetAll}
          pagination={{
            onChange: (page: any, size: any) => {
              setPage(page);
              setSize(size);
            },
            showSizeChanger: true,
            pageSizeOptions: [10, 20, 30],
            current: page,
            pageSize: size,
            total: total,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} of ${total} views`,
          }}
        />
      </ContentBoxShadow>

      {/* <ModalUpdatePermission
        visible={visibleUpdatePermission}
        onCancel={handleCloseUpdatePermission}
        permissionUpdate={permissionsUpdate}
      /> */}
    </LayoutDashboard>
  );
}

export default ViewPage;
