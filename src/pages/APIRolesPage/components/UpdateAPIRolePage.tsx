import { Button, Form, Input, Skeleton, Space, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { APIRolesService } from 'service/APIRolesService';
import { APIsService } from 'service/APIsService';
import { CheckPermissionService } from 'service/CheckPermissionService';
import styled from 'styled-components';
import { APIRoleType } from '../interfaces';

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

function UpdateAPIRolePage() {
  const { id } = useParams() as { id: string };
  const [form] = Form.useForm();
  const navigate = useNavigate();

  // APIs

  const [loadingGetAll, setLoadingGetAll] = useState(false);
  const [APIs, setAPIs] = useState([]);

  // API Roles
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [loadingGetDetail, setLoadingGetDetail] = useState(false);
  const [APIRole, setAPIRole] = useState<APIRoleType>({
    name: '',
    description: '',
    apis: [],
  });

  const [selectedRowKeys, setSelectedRowKeys] = useState<any>([]);

  //

  // CHECKING PERMISSION
  const path = '/' + useLocation().pathname.split('/')[1];

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, path);
  };
  //

  const handleCancel = () => {
    navigate(-1);
  };

  const handleFinish = (values: any) => {
    const param = {
      name: values.name,
      description: values.description,
      apis: selectedRowKeys,
      id,
    };

    setLoadingUpdate(true);
    APIRolesService.updateAPIRoleService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Update API Role success');
        handleGetDetailAPIRole();
        handleCancel();
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Update API role failed');
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  const handleGetDetailAPIRole = () => {
    setLoadingGetDetail(true);
    APIRolesService.getDetailAPIRoleService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setAPIRole(res.data.rows[0]);
        const apis = res.data.rows[0].apis;
        setSelectedRowKeys(apis);
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Get detail API Role failed');
      })
      .finally(() => {
        setLoadingGetDetail(false);
      });
  };

  const handleChangeAPIs = (selectedRowKeys: any) => {
    setSelectedRowKeys(selectedRowKeys);
  };

  const handleGetAllAPIs = () => {
    setLoadingGetAll(true);
    APIsService.getAllAPIsServiceNotPagination()
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setAPIs(res.data.rows);
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Get APIs failed');
      })
      .finally(() => {
        setLoadingGetAll(false);
      });
  };

  useEffect(() => {
    handleGetAllAPIs();
  }, []);

  useEffect(() => {
    handleGetDetailAPIRole();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [APIRole]);

  const columns: ColumnsType<any> = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },

    {
      key: 'method',
      title: 'Method',
      dataIndex: 'method',
      filters: [
        {
          text: 'POST',
          value: 'POST',
        },
        {
          text: 'GET',
          value: 'GET',
        },
        {
          text: 'DELETE',
          value: 'DELETE',
        },
        {
          text: 'PUT',
          value: 'PUT',
        },
      ],
      onFilter: (value, record) => record.method.indexOf(value) === 0,
    },

    {
      key: 'path',
      title: 'Path',
      dataIndex: 'path',
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title="Update  API Roles" />
      <TitlePage title="Update  API Roles" />

      <Wrapper>
        <div className="updateAPI">
          {loadingGetDetail ? (
            <Skeleton />
          ) : (
            <Form
              {...layout}
              form={form}
              className="updateAPI-form"
              onFinish={handleFinish}
            >
              <Form.Item
                name="name"
                label="Name"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={APIRole.name}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="description"
                label="Description"
                labelAlign="left"
                rules={[{ required: true, message: 'This field is require!' }]}
                initialValue={APIRole.description}
              >
                <Input.TextArea rows={4} />
              </Form.Item>

              <Form.Item name="api" label="APIs" labelAlign="left">
                <Table
                  rowKey={'id'}
                  size="small"
                  columns={columns}
                  dataSource={APIs}
                  loading={loadingGetAll}
                  rowSelection={{
                    type: 'checkbox',
                    selectedRowKeys: selectedRowKeys,
                    onChange: handleChangeAPIs,
                  }}
                  pagination={false}
                />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                <Space>
                  {handleGetAction('update') && (
                    <Button
                      type="primary"
                      htmlType="submit"
                      loading={loadingUpdate}
                    >
                      Update
                    </Button>
                  )}

                  <Button type="default" onClick={handleCancel}>
                    Cancel
                  </Button>
                </Space>
              </Form.Item>
            </Form>
          )}
        </div>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default UpdateAPIRolePage;
const Wrapper = styled.div`
  .updateAPI {
    background-color: #fff;
    padding: 12px;
    box-shadow: ${({ theme }) => theme.shadowContainer};
    margin-bottom: 12px;

    &-form {
      /* max-width: 600px; */
    }
  }
`;
