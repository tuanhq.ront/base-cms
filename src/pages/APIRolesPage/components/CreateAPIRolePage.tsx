import { Button, Form, Input, Space, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { APIRolesService } from 'service/APIRolesService';
import { APIsService } from 'service/APIsService';
import styled from 'styled-components';

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

function CreateAPIRolePage() {
  const [form] = Form.useForm();
  const navigate = useNavigate();

  // APIs

  const [loadingGetAll, setLoadingGetAll] = useState(false);
  const [APIs, setAPIs] = useState([]);

  // API Roles

  const [loadingCreate, setLoadingCreate] = useState(false);

  const [selectedRowKeys, setSelectedRowKeys] = useState<any>([]);

  const handleCancel = () => {
    navigate(-1);
  };

  const handleFinish = (values: any) => {
    const param = {
      name: values.name,
      description: values.description,
      apis: selectedRowKeys,
    };

    setLoadingCreate(true);
    APIRolesService.postAPIRoleService(param)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Create API Role success');
        navigate('/api-roles');
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Create API role failed');
      })
      .finally(() => {
        setLoadingCreate(false);
      });
  };

  const handleChangeAPIs = (selectedRowKeys: any) => {
    setSelectedRowKeys(selectedRowKeys);
  };

  const handleGetAllAPIs = () => {
    setLoadingGetAll(true);
    APIsService.getAllAPIsServiceNotPagination()
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setAPIs(res.data.rows);
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Get APIs failed');
      })
      .finally(() => {
        setLoadingGetAll(false);
      });
  };

  useEffect(() => {
    handleGetAllAPIs();
  }, []);

  const columns: ColumnsType<any> = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },

    {
      key: 'method',
      title: 'Method',
      dataIndex: 'method',
    },

    {
      key: 'path',
      title: 'Path',
      dataIndex: 'path',
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title="Create  API Roles" />
      <TitlePage title="Create  API Roles" />

      <Wrapper>
        <div className="createAPI">
          <Form
            {...layout}
            form={form}
            className="createAPI-form"
            onFinish={handleFinish}
          >
            <Form.Item
              name="name"
              label="Name"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="description"
              label="Description"
              labelAlign="left"
              rules={[{ required: true, message: 'This field is require!' }]}
            >
              <Input.TextArea rows={4} />
            </Form.Item>

            <Form.Item name="api" label="APIs" labelAlign="left">
              <Table
                rowKey={'id'}
                size="small"
                columns={columns}
                dataSource={APIs}
                loading={loadingGetAll}
                scroll={{ y: 400 }}
                rowSelection={{
                  type: 'checkbox',
                  selectedRowKeys: selectedRowKeys,
                  onChange: handleChangeAPIs,
                }}
                pagination={false}
              />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loadingCreate}
                >
                  Create
                </Button>
                <Button type="default" onClick={handleCancel}>
                  Cancel
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </div>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default CreateAPIRolePage;
const Wrapper = styled.div`
  .createAPI {
    background-color: #fff;
    padding: 12px;
    box-shadow: ${({ theme }) => theme.shadowContainer};
    margin-bottom: 12px;

    &-form {
      /* max-width: 600px; */
    }
  }
`;
