export interface APIRoleType {
  name: string;
  description: string;
  apis: any[];
}
