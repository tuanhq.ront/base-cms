import { PlusOutlined } from '@ant-design/icons';
import { Button, Popconfirm, Space, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { APIRolesService } from 'service/APIRolesService';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';
import styled from 'styled-components';

function APIRolesPage() {
  const navigate = useNavigate();

  const [loadingGetAll, setLoadingGetAll] = useState(false);
  const [APIRoles, setAPIRoles] = useState([]);

  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);
  const [total, setTotal] = useState(0);

  // CHECKING PERMISSION
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  const handleCheckAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, location.pathname);
  };
  //

  const handleCreateAPIRoles = () => {
    navigate('/api-roles/create');
  };

  const handleUpdateAPIRole = (id: string) => {
    navigate('/api-roles/update/' + id);
  };

  const handleGetAllAPIRoles = () => {
    setLoadingGetAll(true);
    APIRolesService.getAllAPIRolesService(page, size)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        setAPIRoles(res.data.rows);
        setTotal(res.data.total);
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Get list API Roles failed');
      })
      .finally(() => {
        setLoadingGetAll(false);
      });
  };

  const handleDeleteAPIRoles = (id: string) => {
    APIRolesService.deleteAPIRoleService(id)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        NotificationSuccess('Success', 'Delete API Roles success');
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Delete API Roles failed');
      })
      .finally(() => {
        handleGetAllAPIRoles();
      });
  };

  useEffect(() => {
    handleGetAllAPIRoles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, size]);

  const columns: ColumnsType<any> = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'name',
      title: 'Name',
      dataIndex: 'name',
    },
    {
      key: 'description',
      title: 'Description',
      dataIndex: 'description',
    },
    {
      key: 'action',
      title: 'Action',
      width: 200,
      align: 'center',
      render: (text: any, record: any) => (
        <Space>
          <Button
            size="small"
            type="primary"
            onClick={() => handleUpdateAPIRole(record.id)}
          >
            Detail
          </Button>

          {handleCheckAction('delete') && (
            <Popconfirm
              placement="top"
              title="Are you sure to delete this API Role?"
              okText="Yes"
              cancelText="No"
              onConfirm={() => handleDeleteAPIRoles(record.id)}
            >
              <Button size="small" type="primary" danger>
                Delete
              </Button>
            </Popconfirm>
          )}
        </Space>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />

      <Wrapper>
        {handleCheckAction('create') && (
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleCreateAPIRoles}
          >
            Create
          </Button>
        )}

        <div className="APIRoles-table">
          <Table
            rowKey={'id'}
            size="small"
            columns={columns}
            dataSource={APIRoles}
            loading={loadingGetAll}
            pagination={{
              onChange: (page: any, size: any) => {
                setPage(page);
                setSize(size);
              },
              showSizeChanger: true,
              pageSizeOptions: [10, 20, 30],
              current: page,
              pageSize: size,
              total: total,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} items`,
            }}
          />
        </div>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default APIRolesPage;

const Wrapper = styled.div`
  .APIRoles {
    &-table {
      margin-top: 12px;
      background-color: #fff;
      padding: 12px;
      box-shadow: ${({ theme }) => theme.shadowContainer};
      margin-bottom: 12px;
    }

    &-containerTagsRole {
      display: flex;
      justify-content: flex-start;
      align-items: center;
    }

    &-tagsRole {
      border: 1px solid #ccc;
      padding: 0px 5px;
      margin-right: 4px;
      border-radius: 4px;
    }
  }
`;
