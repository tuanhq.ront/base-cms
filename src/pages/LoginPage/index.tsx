import { Button, Form, Input } from 'antd';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import HelmetComponent from '../../components/HelmetComponent';
import {
  NotificationError,
  NotificationSuccess,
} from '../../components/Notification';
import { LoginService } from '../../service/LoginService';

import LOGIN_BACKGROUND from './assets/bg-login.jpeg';

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 10 },
};

function LoginPage() {
  const navigate = useNavigate();
  const [loading, setloading] = useState(false);

  const onFinish = (values: any) => {
    setloading(true);
    LoginService.loginAPI(values.email, values.password)

      .then(res => {
        if (res.data.code !== 200) {
          NotificationError('Error', res.data.message);
          return;
        }

        LoginService.setDataLocalStorage(res.data);

        NotificationSuccess('Success', 'Login success');
        navigate('/dashboard');
      })
      .catch(err => {
        NotificationError('Error', 'Login failed');
      })
      .finally(() => {
        setloading(false);
      });
  };

  return (
    <Wrapper>
      <HelmetComponent title="Login" />

      <div className="loginContainer">
        <div className="loginImg">
          <div className="loginImg-title">
            Blockchain Network Node Management
          </div>
        </div>
        <div className="loginForm">
          <div className="loginForm-title">LOGIN</div>

          <Form onFinish={onFinish} autoComplete="off" {...layout}>
            <Form.Item
              label="Email"
              name="email"
              labelAlign="left"
              rules={[
                { required: true, message: 'Please input your username!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              labelAlign="left"
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 18 }}>
              <Button type="primary" htmlType="submit" loading={loading}>
                Login
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </Wrapper>
  );
}

export default LoginPage;

const Wrapper = styled.div`
  .loginContainer {
    height: 100vh;
    display: flex;
  }

  .loginImg {
    width: 50%;
    background-image: url(${LOGIN_BACKGROUND});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;

    &-title {
      color: #ffffff;
      font-size: 35px;
      font-weight: 500;
      text-align: center;
      margin-top: 100px;
    }
  }

  .loginForm {
    padding: 20px;
    width: 50%;
    background-color: #002140;
    color: #ffffff;
    .ant-form-item-label > label {
      color: #fff;
    }
  }

  .loginForm-title {
    font-size: 30px;
    font-weight: 500;
    margin-bottom: 20px;
  }
`;
