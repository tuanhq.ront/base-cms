import { Col, Row, Statistic } from 'antd';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import TitlePage from 'components/TitlePage';
import React from 'react';
import { useLocation } from 'react-router-dom';
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';
import styled from 'styled-components';

const data = [
  {
    name: '19/04/2022',
    Request: 2583,
  },
  {
    name: '20/04/2022',
    Request: 3895,
  },
  {
    name: '21/04/2022',
    Request: 4002,
  },
  {
    name: '22/04/2022',
    Request: 5225,
  },
  {
    name: '23/04/2022',
    Request: 3574,
  },
  {
    name: '24/04/2022',
    Request: 5552,
  },
  {
    name: '25/04/2022',
    Request: 4367,
  },
];

function DashboardAdminPage() {
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />
      <Wrapper>
        <Row gutter={[8, 8]}>
          <Col lg={6}>
            <div className="dashboard-card dashboard-card__1">
              <div className="dashboard-card__title">Nodes online</div>
              <div className="dashboard-card__content">3</div>
            </div>
          </Col>
          <Col lg={6}>
            <div className="dashboard-card dashboard-card__2">
              <div className="dashboard-card__title">Node validator</div>
              <div className="dashboard-card__content">1</div>
            </div>
          </Col>
          <Col lg={6}>
            <div className="dashboard-card dashboard-card__3">
              <div className="dashboard-card__title">Block Height</div>
              <div className="dashboard-card__content">
                <Statistic value={2317225} />
              </div>
            </div>
          </Col>
          <Col lg={6}>
            <div className="dashboard-card dashboard-card__4">
              <div className="dashboard-card__title">Avg block time</div>
              <div className="dashboard-card__content">0.6061 s</div>
            </div>
          </Col>
        </Row>

        <div className="dashboard-request">
          <div className="dashboard-request-title">Requests</div>
          <ResponsiveContainer width="100%" height={300}>
            <LineChart
              data={data}
              margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
              }}
            >
              <CartesianGrid strokeDasharray="5" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Line type="monotone" dataKey="Request" stroke="#002140" />
            </LineChart>
          </ResponsiveContainer>
        </div>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default DashboardAdminPage;

const Wrapper = styled.div`
  .dashboard-request {
    margin: 10px 0px;
    background-color: #fff;
    padding: 12px;
    box-shadow: #0000003b 2.4px 2.4px 3.2px;

    &-title {
      text-align: center;
      font-size: 20px;
      padding-bottom: 10px;
      font-weight: 500;
    }
  }

  .dashboard-card {
    height: 100px;
    padding: 12px;
    background-color: #fff;
    box-shadow: #0000003b 2.4px 2.4px 3.2px;
    margin-bottom: 10px;

    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-between;

    &__1 {
      background-image: linear-gradient(68.4deg, #63fbd7 -0.4%, #05defa 100.2%);
    }
    &__2 {
      background-image: radial-gradient(
        circle farthest-corner at 7.2% 13.6%,
        #25f9f5 0%,
        #8caaf1 90%
      );
    }
    &__3 {
      background-image: linear-gradient(359.8deg, #fcffde 2.2%, #b6f1ab 99.3%);
    }
    &__4 {
      background-image: radial-gradient(
        circle 654px at 0.6% 48%,
        #0caaff 0%,
        #97ff81 86.3%
      );
    }

    &__title {
      font-size: 16px;
      font-weight: 500;
    }

    &__content {
      font-size: 20px;
      font-weight: 500;
    }
  }
`;
