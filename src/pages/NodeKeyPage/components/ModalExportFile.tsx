import { Input, Modal } from 'antd';
import React, { useState } from 'react';

interface Props {
  isVisible: boolean;
  onClose: () => void;
  callback: (name: string) => void;
}

function ModalExportFile(props: Props) {
  const { isVisible, onClose, callback } = props;
  const [name, setName] = useState('');

  const handleCancel = () => {
    onClose();
  };

  const handleOk = () => {
    callback(name.trim());
  };

  return (
    <Modal
      title="Input filename"
      visible={isVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      maskClosable={false}
      okText="Export"
    >
      Filename:
      <Input
        value={name}
        onChange={e => {
          setName(e.target.value);
        }}
        addonAfter=".json"
        placeholder="Ex: nodeKey"
      ></Input>
    </Modal>
  );
}

export default ModalExportFile;
