import {
  ClearOutlined,
  CopyOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';
import { Button, Form, Input, Modal, Popconfirm, Space, Table } from 'antd';
import HelmetComponent from 'components/HelmetComponent';
import LayoutDashboard from 'components/Layouts/LayoutDashboard';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import TitlePage from 'components/TitlePage';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { CheckPermissionService } from 'service/CheckPermissionService';
import { DataViewTypes } from 'service/interfaces';
import { LoginService } from 'service/LoginService';
import { NodeKeyServices } from 'service/NodeKeyServices';
import styled from 'styled-components';
import { ContentBoxShadow } from 'styledComponents/commonStyled';
import ModalExportFile from './components/ModalExportFile';

const { KeyPair } = require('near-api-js');

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

function NodeKeyPage() {
  const dataAccount = LoginService.getDataLocalStorage();
  const [form] = Form.useForm();

  const [key, setKey] = useState({ publicKey: '', privateKey: '' });

  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);
  const [keys, setKeys] = useState([]);
  const [loadingGetList, setLoadingGetList] = useState(false);

  const [loadingUploadKey, setLoadingUploadKey] = useState(false);

  //
  const [visibleModalExport, setVisibleModalExport] = useState(false);

  // CHECKING PERMISSION
  const location = useLocation();
  const dataView: DataViewTypes = CheckPermissionService.getDataView(
    location.pathname,
  );

  const handleGetAction = (actionKey: string): string | null => {
    return CheckPermissionService.getAction(actionKey, location.pathname);
  };

  //
  const handleOpenModalExport = () => {
    setVisibleModalExport(true);
  };

  const handleCloseModalExport = () => {
    setVisibleModalExport(false);
  };

  const callbackModalExport = (name: string) => {
    handleExportKey(name);
  };

  const onFinish = (values: any) => {
    const param = {
      public_key: values.publicKeyUpload,
      organization_id: dataAccount.organization_id,
      organization_name: dataAccount.organization_name,
    };

    Modal.confirm({
      title: 'Confirm upload key',
      icon: <ExclamationCircleOutlined />,
      content: 'Are you sure to upload key?',
      okText: 'Yes',
      cancelText: 'Cancel',
      onOk: () => {
        setLoadingUploadKey(true);
        NodeKeyServices.uploadKey(param)
          .then((res: any) => {
            if (res.data.code !== 0) {
              NotificationError('Error', res.data.message);
              return;
            }
            NotificationSuccess('Success', 'Upload key success');
            handleGetNodeKeysOfOrganization();
          })
          .catch((err: any) => {
            NotificationError('Error', 'Upload key failed');
          })
          .finally(() => {
            setLoadingUploadKey(false);
          });
      },
    });
  };

  const handleGenerateKey = () => {
    const newKeyPair = KeyPair.fromRandom('ed25519');
    newKeyPair.public_key = newKeyPair.publicKey
      .toString()
      .replace('ed25519:', '');
    const publicKey = 'ed25519:' + newKeyPair.public_key;
    const privateKey = 'ed25519:' + newKeyPair.secretKey;

    setKey({ publicKey, privateKey });
    form.setFields([
      { name: 'publicKey', value: publicKey.substring(0, 15) + ' ...' },
      { name: 'publicKeyUpload', value: publicKey },
      { name: 'privateKey', value: privateKey.substring(0, 20) + ' ...' },
    ]);
    NotificationSuccess(
      'Success',
      'Generate key success. Click Export key to save key.',
    );
  };

  const handleExportKey = (name: string) => {
    if (key.publicKey === '' || key.privateKey === '') {
      NotificationError('Error', 'Generate key first');
      return;
    }

    const param = JSON.stringify({ ...key });

    const blob = new Blob([param], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${name ? name : 'nodeKey'}.json`);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);

    // form.resetFields();
    // setKey({ publicKey: '', privateKey: '' });
  };

  const handleGetNodeKeysOfOrganization = () => {
    setLoadingGetList(true);
    NodeKeyServices.getNodeKeysOfOrganization(page, size)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }
        setKeys(res.data.rows);
        setTotal(res.data.total);
        // NotificationSuccess('Success', res.data.message);
      })
      .catch((err: any) => {
        console.error(err);
        NotificationError('Error', 'Get list keys failed');
      })
      .finally(() => {
        setLoadingGetList(false);
      });
  };

  const handleCopyClipboard = () => {
    if (!key.publicKey) {
      NotificationError('Error', 'Generate key first');
      return;
    }
    navigator.clipboard.writeText(key.publicKey);
    NotificationSuccess('Success', 'Copy public key to clipboard');
  };

  const handleClearPublicKey = () => {
    form.setFields([{ name: 'publicKeyUpload', value: '' }]);
  };

  useEffect(() => {
    handleGetNodeKeysOfOrganization();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, size]);

  const columns: any = [
    {
      key: '#',
      title: '#',
      width: 40,
      align: 'center',
      render: (text: any, record: any, index: any) => <>{index + 1}</>,
    },
    {
      key: 'public_key',
      title: 'Public key',
      dataIndex: 'public_key',
      ellipsis: true,
    },
    {
      key: 'create_at',
      title: 'Create at',
      dataIndex: 'create_at',
      width: 200,
      render: (text: any) => {
        return (
          <>{moment(new Date(parseInt(text))).format('DD/MM/YYYY HH:mm:ss')}</>
        );
      },
    },
    {
      key: 'key_status',
      title: 'Status',
      dataIndex: 'key_status',
      width: 160,
      render: (text: any) => (
        <div className={`keyTag keyTag-${text}`}>
          {text === 0 && 'Pending'}
          {text === 1 && 'Approve'}
          {text === 2 && 'Reject'}
          {text === 3 && 'Revoke'}
        </div>
      ),
    },
  ];

  return (
    <LayoutDashboard>
      <HelmetComponent title={dataView.title} />
      <TitlePage title={dataView.title} />
      <Wrapper>
        <Form form={form} {...layout} className="mt-10" onFinish={onFinish}>
          <div className="nodeKey">
            <div className="nodeKey-label">Generate key</div>
            <Form.Item name="privateKey" label="Private key:" labelAlign="left">
              <Input.TextArea
                disabled
                style={{ maxWidth: '600px' }}
                rows={2}
                className="mt-10"
              ></Input.TextArea>
            </Form.Item>

            <Form.Item name="publicKey" label="Pubic key:" labelAlign="left">
              <Input
                disabled
                style={{ maxWidth: '600px' }}
                className="mt-10"
                suffix={
                  <Button
                    type="link"
                    icon={<CopyOutlined />}
                    onClick={handleCopyClipboard}
                  />
                }
              ></Input>
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              {handleGetAction('generate-key') && (
                <Popconfirm
                  placement="bottom"
                  title="Do you want to generate new key?"
                  onConfirm={handleGenerateKey}
                  okText="Yes"
                  cancelText="No"
                  disabled={key.publicKey === ''}
                >
                  <Button
                    type="primary"
                    onClick={() => {
                      if (key.publicKey !== '') {
                        return;
                      }
                      handleGenerateKey();
                    }}
                  >
                    Generate key
                  </Button>
                </Popconfirm>
              )}
            </Form.Item>
          </div>

          <div className="nodeKey">
            <div className="nodeKey-label">Upload key</div>

            <Form.Item
              name="publicKeyUpload"
              rules={[
                {
                  required: true,
                  message: 'This field is require!',
                },
              ]}
              label="Pubic key upload:"
              labelAlign="left"
            >
              <Input
                style={{ maxWidth: '600px' }}
                className="mt-10"
                suffix={
                  <Button
                    type="link"
                    onClick={handleClearPublicKey}
                    icon={<ClearOutlined />}
                  />
                }
              ></Input>
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              <Space>
                {handleGetAction('upload-key') && (
                  <Button
                    type="primary"
                    htmlType="submit"
                    loading={loadingUploadKey}
                  >
                    Upload key
                  </Button>
                )}

                {handleGetAction('export-key') && (
                  <Button
                    onClick={handleOpenModalExport}
                    icon={
                      <i
                        className="fa-solid fa-file-export"
                        style={{ marginRight: '4px' }}
                      ></i>
                    }
                  >
                    Export key
                  </Button>
                )}
              </Space>
            </Form.Item>
          </div>
        </Form>

        <ContentBoxShadow>
          <div className="nodeKey-label">Node keys upload</div>
          <Table
            rowKey={'public_key'}
            size="small"
            columns={columns}
            dataSource={keys}
            loading={loadingGetList}
            pagination={{
              onChange: (page: any, size: any) => {
                setPage(page);
                setSize(size);
              },
              showSizeChanger: true,
              pageSizeOptions: [10, 20, 30],
              current: page,
              pageSize: size,
              total: total,
              showTotal: (total, range) =>
                `${range[0]}-${range[1]} of ${total} items`,
            }}
          />
        </ContentBoxShadow>

        <ModalExportFile
          isVisible={visibleModalExport}
          onClose={handleCloseModalExport}
          callback={callbackModalExport}
        ></ModalExportFile>
      </Wrapper>
    </LayoutDashboard>
  );
}

export default NodeKeyPage;

const Wrapper = styled.div`
  .nodeKey {
    background-color: #fff;
    padding: 12px;
    box-shadow: ${({ theme }) => theme.shadowContainer};

    margin-bottom: 12px;

    .ant-input-disabled {
      color: #f00;
      font-style: italic;
    }

    &-label {
      font-weight: 500;
      font-size: 16px;
    }
  }

  .keyTag {
    padding: 0px 8px;
    width: 100px;
    text-align: center;
    border-radius: 4px;
  }

  .keyTag-Approved {
    background-color: #00ff002e;
    color: #00b300;
  }

  .keyTag-Pending {
    background-color: #f1f1f1;
    color: #000000;
  }

  .keyTag-Rejected {
    background-color: #ff00002e;
    color: #ff0000;
  }
`;
