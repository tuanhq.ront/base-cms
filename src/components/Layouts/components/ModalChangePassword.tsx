import { Button, Form, Input, Modal, Space } from 'antd';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { LoginService } from 'service/LoginService';

interface Props {
  visible: boolean;
  onClose: () => void;
}

const layout = {
  labelCol: { span: 10 },
  wrapperCol: { span: 14 },
};

function ModalChangePassword(props: Props) {
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const { visible, onClose } = props;

  const [loading, setLoading] = useState(false);

  const handleCancel = () => {
    onClose();
  };

  const onFinish = (values: any) => {
    const params = {
      newPassword: values.newPassword,
      oldPassword: values.oldPassword,
    };

    LoginService.changePasswordAccount(params)
      .then(res => {
        if (res.data.code !== 0) {
          NotificationError('Error', res.data.message);
          return;
        }

        navigate('/login');
        handleLogout();
        onClose();
        NotificationSuccess('Success', 'Change password success. Login again.');
      })
      .catch(err => {
        console.error(err);
        NotificationError('Error', 'Change password failed');
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleLogout = () => {
    LoginService.logoutAPI()
      .then(res => {})
      .catch(res => {
        console.error(res);
      });
  };

  return (
    <Modal
      title="Change password account"
      visible={visible}
      onCancel={handleCancel}
      maskClosable={false}
      footer={false}
    >
      <Form {...layout} form={form} onFinish={onFinish}>
        <Form.Item
          label="Old password"
          rules={[
            {
              required: true,
              message: 'This field is require!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (value === undefined || value.trim() === '') {
                  return Promise.reject();
                }

                if (value.length < 6) {
                  return Promise.reject(
                    new Error('Password must be at least 6 characters'),
                  );
                }

                return Promise.resolve();
              },
            }),
          ]}
          name="oldPassword"
          labelAlign="left"
          hasFeedback
        >
          <Input.Password autoComplete="new-password"></Input.Password>
        </Form.Item>

        <Form.Item
          label="New password"
          rules={[
            {
              required: true,
              message: 'This field is require!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (value === undefined || value.trim() === '') {
                  return Promise.reject();
                }

                if (value.length < 6) {
                  return Promise.reject(
                    new Error('Password must be at least 6 characters'),
                  );
                }

                return Promise.resolve();
              },
            }),
          ]}
          name="newPassword"
          labelAlign="left"
          hasFeedback
        >
          <Input.Password autoComplete="new-password"></Input.Password>
        </Form.Item>

        <Form.Item
          label="Confirm new password"
          rules={[
            {
              required: true,
              message: 'This field is require!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (value === undefined || value.trim() === '') {
                  return Promise.reject();
                }

                if (value.length < 6) {
                  return Promise.reject(
                    new Error('Password must be at least 6 characters'),
                  );
                }

                if (value !== getFieldValue('newPassword')) {
                  return Promise.reject(new Error('Password does not match'));
                }

                return Promise.resolve();
              },
            }),
          ]}
          name="confirmNewPassword"
          labelAlign="left"
          hasFeedback
        >
          <Input.Password></Input.Password>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 10, span: 14 }}>
          <Space>
            <Button type="primary" htmlType="submit" loading={loading}>
              Confirm
            </Button>
            <Button onClick={handleCancel}>Cancel</Button>
          </Space>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default ModalChangePassword;
