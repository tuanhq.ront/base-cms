import { SettingOutlined } from '@ant-design/icons';
import { Button, Popover } from 'antd';
import {
  NotificationError,
  NotificationSuccess,
} from 'components/Notification';
import React, { ReactNode, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import { LoginService } from '../../service/LoginService';
import LeftMenu from '../LeftMenu';
import LogoGalaxy from './assets/galaxy.svg';
import ModalChangePassword from './components/ModalChangePassword';

interface Props {
  children: ReactNode;
}

function LayoutDashboard(props: Props) {
  const { children } = props;
  const navigate = useNavigate();

  const [openChangePassword, setOpenChangePassword] = useState(false);

  const handleOpenChangePassword = () => {
    setOpenChangePassword(true);
  };

  const handleCloseChangePassword = () => {
    setOpenChangePassword(false);
  };

  const data = LoginService.getDataLocalStorage();

  const handleLogout = () => {
    LoginService.logoutAPI()
      .then(res => {
        NotificationSuccess('Success', 'Logout success');
        navigate('/login');
      })
      .catch(res => {
        console.error(res);
        NotificationError('Error', 'Logout failed');
      });
  };

  const text = <TextStyled>{data.email}</TextStyled>;

  const content = (
    <ContentStyled>
      <div>Organization: {data.organization_name}</div>

      <div className="layout-containerButton">
        <Button type="default" onClick={handleOpenChangePassword}>
          Change Password
        </Button>
        <Button type="default" danger onClick={handleLogout}>
          Log out
        </Button>
      </div>
    </ContentStyled>
  );
  return (
    <Wrapper>
      <div className="layout-header">
        <div className="layout-header-logo">
          <img src={LogoGalaxy} alt="logo" />
        </div>
        <div className="layout-header-account">
          <Popover
            placement="bottomLeft"
            title={text}
            content={content}
            trigger="click"
          >
            <Button
              className="layout-header-setting"
              type="link"
              icon={<SettingOutlined />}
            >
              Account
            </Button>
          </Popover>
        </div>
      </div>
      <div className="layout-container">
        {/* <div className="layout-leftMenu"></div> */}
        <LeftMenu />

        <div className="layout-content">{children}</div>
      </div>

      <ModalChangePassword
        visible={openChangePassword}
        onClose={handleCloseChangePassword}
      ></ModalChangePassword>
    </Wrapper>
  );
}

export default LayoutDashboard;

const ContentStyled = styled.div`
  width: 250px;

  .layout-containerButton {
    margin-top: 10px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;

const TextStyled = styled.div`
  text-align: center;
`;

const Wrapper = styled.div`
  .layout {
    &-header {
      width: 100%;
      height: 60px;
      background-color: ${({ theme }) => theme.background2};
      padding: 0px 16px;

      display: flex;
      align-items: center;
      justify-content: space-between;

      &-logo {
        width: 130px;
        text-align: center;

        & > img {
          height: 55px;
        }
      }

      &-setting {
        color: ${({ theme }) => theme.text2};
        font-size: 16px;
        font-weight: 500;
      }
    }

    &-container {
      display: flex;
    }

    &-leftMenu {
      min-height: calc(100vh - 60px);
      background-color: ${({ theme }) => theme.background3};
      /* border-top: 1px solid ${({ theme }) => theme.border}; */

      color: ${({ theme }) => theme.text2};
    }

    &-content {
      padding: 16px;
      width: 100%;
      background-color: ${({ theme }) => theme.background5};

      height: calc(100vh - 60px);
      overflow-y: auto;
    }
  }
`;
