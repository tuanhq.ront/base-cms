import { Spin } from 'antd';
import React from 'react';
import styled from 'styled-components';

function Loading() {
  return (
    <Wrapper>
      <Spin size="large" />
    </Wrapper>
  );
}

export default Loading;

const Wrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  background: rgba(0, 0, 0, 0.2);
  justify-content: center;
  align-items: center;
  z-index: 9999;
`;
