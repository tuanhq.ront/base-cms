import { UserOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import React, { useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import styled from 'styled-components';
import { LoginService } from '../../service/LoginService';

const { Sider } = Layout;
// const { SubMenu } = Menu;

function LeftMenu() {
  const location = useLocation();

  const activeKey = location.pathname.split('/')[1];

  const dataAccount = LoginService.getDataLocalStorage();

  const newPermissions = dataAccount.permission.map((e: any) => ({
    ...e,
    children: e.actions,
  }));

  const [collapsed, setCollapsed] = useState(false);

  const onCollapse = (params: any) => {
    setCollapsed(params);
  };

  return (
    <Wrapper>
      <Sider
        className="sider-container"
        collapsible
        collapsed={collapsed}
        onCollapse={onCollapse}
      >
        <Menu theme="dark" defaultSelectedKeys={[activeKey]} mode="inline">
          <Menu.Item
            key="null"
            className="sider-account"
            icon={<UserOutlined style={{ fontSize: '0px' }} />}
          >
            <div>
              <div className="sider-account-name">{dataAccount.full_name}</div>
              <div className="sider-account-organization">
                {dataAccount.organization_name}
              </div>
            </div>
          </Menu.Item>
          {newPermissions.map((link: any, index: any) => (
            <Menu.Item
              key={link.path.substring(1)}
              icon={<div dangerouslySetInnerHTML={{ __html: link.icon }} />}
            >
              <Link key={index} to={link.path}>
                {link.title}
              </Link>
            </Menu.Item>
          ))}

          {/* <SubMenu key="sub2" icon={<TeamOutlined />} title="Team">
            <Menu.Item key="6">Team 1asdfasdfasdfasdfadsf</Menu.Item>
            <Menu.Item key="8">Team 2</Menu.Item>
          </SubMenu> */}
        </Menu>
      </Sider>
    </Wrapper>
  );
}

export default LeftMenu;

const Wrapper = styled.div`
  .sider {
    &-container {
      overflow-y: auto;
      height: calc(100vh - 60px);

      /* width */
      ::-webkit-scrollbar {
        width: 5px;
      }

      /* Track */
      ::-webkit-scrollbar-track {
        background: #f1f1f1;
      }

      /* Handle */
      ::-webkit-scrollbar-thumb {
        background: #001529;
      }

      /* Handle on hover */
      ::-webkit-scrollbar-thumb:hover {
        background: #555;
      }
    }

    &-account {
      height: 60px;
      padding-left: 14px !important;
      text-align: center;

      &-name {
        margin-left: -8px;
        font-weight: bold;
        color: #fff;
        font-size: 16px;
        height: 20px;
      }
      &-organization {
        margin-left: -8px;
        color: #fff;
      }
    }

    &-img {
      height: 52px;
      width: 52px;
      border: 1px solid #ccc;
      border-radius: 30px;
    }
  }
`;
