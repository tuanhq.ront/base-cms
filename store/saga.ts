import ItemsSaga from 'pages/ItemsPage/redux/saga';
import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([ItemsSaga()]);
}
